'data layer class - takes care of all data operations and also provides some generic helper functions

Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.IO
Imports Microsoft.SqlServer.Server
Imports System.Configuration

Public NotInheritable Class Core
    Public Enum enmModes
        Add
        Edit
        Save
        SaveClose
        Delete
    End Enum

    Public Enum enmStatus
        StatusError
        StatusInfo
    End Enum

    Private Sub New()
        'constructor - private means it can't be created
    End Sub

    Public Shared Sub RunProgramme(ByVal DocName As String)
        Dim command As New System.Diagnostics.Process()
        command.StartInfo.FileName = DocName
        command.StartInfo.UserName = My.Settings.File_Username
        command.StartInfo.Domain = My.Settings.File_Domain
        Dim pwd As New System.Security.SecureString()

        For Each c As Char In My.Settings.File_Password
            pwd.AppendChar(c)
        Next

        command.StartInfo.Password = pwd
        command.StartInfo.CreateNoWindow = False
        command.StartInfo.Verb = "open"
        command.StartInfo.UseShellExecute = False

        Try
            command.Start()
        Catch e11 As Win32Exception

            'Response.Write(e11.Message)
        End Try
        command.WaitForExit()
        command.Close()
    End Sub

    Public Shared Function OpenDocument(ByVal DocName As String) As Boolean
        'Opens a document in it's associated application
        'also works with web pages.

        'Returns true if successful, false otherwise
        'Examples:
        'OpenDocument("C:\MyDocument.doc")
        'OpenDocument("http://www.freevbcode.com")
        'Verified to work in VB.NET Version 1.0, 04/01/02.


        Try
            System.Diagnostics.Process.Start(DocName)
            Return True
        Catch
            Return False
        End Try

    End Function

    Public Shared Function fnConnectionString() As String
        Return My.Settings.ConnectionString
    End Function
    Public Shared Function fnConnectionStringHD() As String
        Return My.Settings.ConnectionStringHD
    End Function

    Public Shared Function data_select(ByVal SQL As String, ByVal ConnectionString As String) As Data.DataTable
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlClient.SqlConnection(ConnectionString)
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlClient.SqlCommand(SQL, sqlCon)
        'create a new data adapter
        Dim adpAdapter As New SqlClient.SqlDataAdapter
        'create a new dataset
        Dim datSet As New System.Data.DataSet

        Try
            sqlCmd.CommandTimeout = 300
            'set the data adapter command object1
            adpAdapter.SelectCommand = sqlCmd
            'open the connection
            sqlCon.Open()
            'fill the dataset
            adpAdapter.Fill(datSet, "Main")
            'return just the main datatable (more lightweight object)
            Return datSet.Tables("Main")
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function

    Public Shared Function data_select_value(ByVal SQL As String, ByVal ConnectionString As String) As String
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlClient.SqlConnection(ConnectionString)
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlClient.SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCmd.CommandTimeout = 0
            sqlCon.Open()
            'return just the single value
            Return sqlCmd.ExecuteScalar()
        Catch ex As Exception
            Return "0"
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function

    Public Shared Function data_execute_nonquery(ByVal SQL As String, ByVal ConnectionString As String) As Integer
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlClient.SqlConnection(ConnectionString)
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlClient.SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCon.Open()
            'execute the SQL, returning the number of rows affected
            data_execute_nonquery = sqlCmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function

    Public Shared Function data_select_image(ByVal SQL As String, ByVal ImageField As String, ByVal path As String, ByVal ConnectionString As String) As Byte()
        'create a new Connection object using the connection string
        Dim sqlCon As New SqlClient.SqlConnection(ConnectionString)
        'create a new Command using the CommandText and Connection object
        Dim sqlCmd As New SqlClient.SqlCommand(SQL, sqlCon)

        Try
            'open the connection
            sqlCon.Open()
            'get image data
            Dim datImage As SqlClient.SqlDataReader = sqlCmd.ExecuteReader
            datImage.Read()
            If datImage.HasRows Then
                Try
                    'return byte array of image from db
                    Return CType(datImage.Item(ImageField), Byte())
                Catch ex As Exception
                    Return Nothing
                End Try
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        Finally
            'clean up
            sqlCon.Close()
            sqlCon.Dispose()
        End Try
    End Function

    'return 0 if object is null, else decimal value
    Public Shared Function CheckForNullDecimal(ByVal o As Object) As Decimal
        If (o Is Nothing) Or (IsDBNull(o)) Then
            Return 0
        Else
            Return CType(o, Decimal)
        End If
    End Function

    'return 0 if null, else integer value of object.
    Public Shared Function CheckForNullInteger(ByVal i As Object) As Integer
        If (i Is Nothing) Or (IsDBNull(i)) Then
            Return 0
        Else
            Return CType(i, Integer)
        End If
    End Function

    'return String if object is not null, else return empty.string
    Public Shared Function CheckForNullString(ByVal s As Object) As String
        If (s Is Nothing) Or (IsDBNull(s)) Then
            Return String.Empty
        Else
            Return CType(s, String)
        End If
    End Function

    'return empty string if object is null, else decimal value
    Public Shared Function CheckForNullDate(ByVal d As Object) As String
        If (d Is Nothing) Or (IsDBNull(d)) Then
            Return String.Empty
        Else
            Return CType(d, String)
        End If
    End Function

    'return false if object is null, else boolean value
    Public Shared Function CheckForNullBoolean(ByVal b As Object) As Boolean
        If (b Is Nothing) Or (IsDBNull(b)) Then
            Return False
        Else
            Return CType(b, Boolean)
        End If
    End Function

    Public Shared Function fnHalfHourDataDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_halfhourdatadirectory_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnDataDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_datadirectory_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnCustomerDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_customerdirectory_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnTyrrellDataDirectory() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_tyrrelldatadirectory_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnHelpdeskURL() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_helpdeskurl_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIURL() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapiurl_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnHelpdeskRESTAPIkey() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_helpdeskrestapikey_latest", fnConnectionString)
        Return _return
    End Function

    Public Shared Function fnSMTPServer() As String
        Dim _return As String = Core.data_select_value("EXEC uml_cms.dbo.rsp_configuration_smtpserver_latest", fnConnectionString)
        Return _return
    End Function


End Class
