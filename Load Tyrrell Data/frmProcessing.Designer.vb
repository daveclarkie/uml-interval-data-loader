﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCountProcessesSelected = New System.Windows.Forms.Label()
        Me.lnkEditSelectedProcesses = New System.Windows.Forms.LinkLabel()
        Me.grpAvailableProcesses = New System.Windows.Forms.GroupBox()
        Me.btnAvailableProcessesDone = New System.Windows.Forms.Button()
        Me.lbAvailableProcesses = New System.Windows.Forms.ListBox()
        Me.lblCurrentStatusLabel = New System.Windows.Forms.Label()
        Me.grpInformation = New System.Windows.Forms.GroupBox()
        Me.lnkCurrentStatus = New System.Windows.Forms.LinkLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.grpAvailableProcesses.SuspendLayout()
        Me.grpInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCountProcessesSelected
        '
        Me.lblCountProcessesSelected.AutoSize = True
        Me.lblCountProcessesSelected.Location = New System.Drawing.Point(46, 27)
        Me.lblCountProcessesSelected.Name = "lblCountProcessesSelected"
        Me.lblCountProcessesSelected.Size = New System.Drawing.Size(39, 13)
        Me.lblCountProcessesSelected.TabIndex = 0
        Me.lblCountProcessesSelected.Text = "Label1"
        '
        'lnkEditSelectedProcesses
        '
        Me.lnkEditSelectedProcesses.AutoSize = True
        Me.lnkEditSelectedProcesses.Location = New System.Drawing.Point(16, 27)
        Me.lnkEditSelectedProcesses.Name = "lnkEditSelectedProcesses"
        Me.lnkEditSelectedProcesses.Size = New System.Drawing.Size(24, 13)
        Me.lnkEditSelectedProcesses.TabIndex = 1
        Me.lnkEditSelectedProcesses.TabStop = True
        Me.lnkEditSelectedProcesses.Text = "edit"
        '
        'grpAvailableProcesses
        '
        Me.grpAvailableProcesses.Controls.Add(Me.btnAvailableProcessesDone)
        Me.grpAvailableProcesses.Controls.Add(Me.lbAvailableProcesses)
        Me.grpAvailableProcesses.Location = New System.Drawing.Point(1000, 499)
        Me.grpAvailableProcesses.Name = "grpAvailableProcesses"
        Me.grpAvailableProcesses.Size = New System.Drawing.Size(387, 217)
        Me.grpAvailableProcesses.TabIndex = 2
        Me.grpAvailableProcesses.TabStop = False
        Me.grpAvailableProcesses.Text = "Available Processes"
        Me.grpAvailableProcesses.Visible = False
        '
        'btnAvailableProcessesDone
        '
        Me.btnAvailableProcessesDone.Location = New System.Drawing.Point(7, 187)
        Me.btnAvailableProcessesDone.Name = "btnAvailableProcessesDone"
        Me.btnAvailableProcessesDone.Size = New System.Drawing.Size(371, 23)
        Me.btnAvailableProcessesDone.TabIndex = 1
        Me.btnAvailableProcessesDone.Text = "Done"
        Me.btnAvailableProcessesDone.UseVisualStyleBackColor = True
        '
        'lbAvailableProcesses
        '
        Me.lbAvailableProcesses.FormattingEnabled = True
        Me.lbAvailableProcesses.Location = New System.Drawing.Point(7, 20)
        Me.lbAvailableProcesses.Name = "lbAvailableProcesses"
        Me.lbAvailableProcesses.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbAvailableProcesses.Size = New System.Drawing.Size(371, 160)
        Me.lbAvailableProcesses.TabIndex = 0
        '
        'lblCurrentStatusLabel
        '
        Me.lblCurrentStatusLabel.AutoSize = True
        Me.lblCurrentStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentStatusLabel.Location = New System.Drawing.Point(16, 50)
        Me.lblCurrentStatusLabel.Name = "lblCurrentStatusLabel"
        Me.lblCurrentStatusLabel.Size = New System.Drawing.Size(55, 13)
        Me.lblCurrentStatusLabel.TabIndex = 3
        Me.lblCurrentStatusLabel.Text = "Status : "
        '
        'grpInformation
        '
        Me.grpInformation.Controls.Add(Me.lnkCurrentStatus)
        Me.grpInformation.Controls.Add(Me.Button1)
        Me.grpInformation.Controls.Add(Me.lnkEditSelectedProcesses)
        Me.grpInformation.Controls.Add(Me.lblCountProcessesSelected)
        Me.grpInformation.Controls.Add(Me.lblCurrentStatusLabel)
        Me.grpInformation.Location = New System.Drawing.Point(12, 12)
        Me.grpInformation.Name = "grpInformation"
        Me.grpInformation.Size = New System.Drawing.Size(494, 280)
        Me.grpInformation.TabIndex = 5
        Me.grpInformation.TabStop = False
        Me.grpInformation.Text = "Information"
        '
        'lnkCurrentStatus
        '
        Me.lnkCurrentStatus.AutoSize = True
        Me.lnkCurrentStatus.Location = New System.Drawing.Point(77, 50)
        Me.lnkCurrentStatus.Name = "lnkCurrentStatus"
        Me.lnkCurrentStatus.Size = New System.Drawing.Size(59, 13)
        Me.lnkCurrentStatus.TabIndex = 6
        Me.lnkCurrentStatus.TabStop = True
        Me.lnkCurrentStatus.Text = "LinkLabel1"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(36, 201)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 301)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(494, 250)
        Me.WebBrowser1.TabIndex = 7
        '
        'frmProcessing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1129, 563)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.grpInformation)
        Me.Controls.Add(Me.grpAvailableProcesses)
        Me.Name = "frmProcessing"
        Me.Text = "frmProcessing"
        Me.grpAvailableProcesses.ResumeLayout(False)
        Me.grpInformation.ResumeLayout(False)
        Me.grpInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblCountProcessesSelected As System.Windows.Forms.Label
    Friend WithEvents lnkEditSelectedProcesses As System.Windows.Forms.LinkLabel
    Friend WithEvents grpAvailableProcesses As System.Windows.Forms.GroupBox
    Friend WithEvents btnAvailableProcessesDone As System.Windows.Forms.Button
    Friend WithEvents lbAvailableProcesses As System.Windows.Forms.ListBox
    Friend WithEvents lblCurrentStatusLabel As System.Windows.Forms.Label
    Friend WithEvents grpInformation As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lnkCurrentStatus As System.Windows.Forms.LinkLabel
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
End Class
