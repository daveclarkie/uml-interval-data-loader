﻿Imports System.Runtime.CompilerServices
Imports System.IO
Imports ADODB

Public Class frmProcessing


    Private Sub frmProcessing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        LoadAvailableProcesses()
        CountProcessesSelected()
        MainProcess()
    End Sub

    Private Sub LoadAvailableProcesses()

        With Me.lbAvailableProcesses
            .DataSource = Core.data_select("SELECT intResourcePK, varResourceName FROM [APP_HalfHourDataLoad].dbo.tblResources WHERE dtmEnded IS NULL")
            .DisplayMember = "varResourceName"
            .ValueMember = "intResourcePK"
            .SelectedIndex = -1
        End With

    End Sub

    Private Sub lbAvailableProcesses_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAvailableProcesses.SelectedIndexChanged

        CountProcessesSelected()

    End Sub



    Private Sub CountProcessesSelected()

        Dim intSelectedProcesses As Integer = Me.lbAvailableProcesses.SelectedItems.Count
        Dim intAvailableProcesses As Integer = Me.lbAvailableProcesses.Items.Count

        Me.lblCountProcessesSelected.Text = "There are " & intSelectedProcesses & " / " & intAvailableProcesses & " processes currently selected."

    End Sub



    Private Sub lnkEditSelectedProcesses_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEditSelectedProcesses.LinkClicked
        Me.grpAvailableProcesses.Visible = True
        Me.grpInformation.Visible = False

        Me.grpAvailableProcesses.Location = New Point(12, 12)
        Me.Size = New System.Drawing.Size(425, 274)
    End Sub

    Private Sub btnAvailableProcessesDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvailableProcessesDone.Click
        Me.grpAvailableProcesses.Visible = False
        Me.grpInformation.Visible = True
        Me.Size = New System.Drawing.Size(944, 539)
        MainProcess()

    End Sub

    Private Sub MainProcess()
        Me.lnkCurrentStatus.Text = Nothing

        For Each index As Integer In lbAvailableProcesses.SelectedIndices
            Dim dtr As System.Data.DataRowView = Nothing
            dtr = lbAvailableProcesses.Items.Item(index)

            Me.lnkCurrentStatus.Text = Core.data_select_value("SELECT Value FROM [APP_HalfHourDataLoad].dbo.uvw_DisplayAvailableResources WHERE ID = " & dtr(0).ToString)
            Me.lnkCurrentStatus.Links(0).LinkData = Core.data_select_value("SELECT varFullOriginalPath FROM [APP_HalfHourDataLoad].dbo.uvw_DisplayAvailableResources WHERE ID = " & dtr(0).ToString)
        Next
    End Sub

    Private Sub lnkCurrentStatus_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCurrentStatus.LinkClicked

        Me.WebBrowser1.Navigate(Me.lnkCurrentStatus.Links(0).LinkData)
        Me.WebBrowser1.Url = New Uri(Me.lnkCurrentStatus.Links(0).LinkData)
    End Sub

    Private Function GetRandomFileName() As String


        Dim r As New Random()
        Dim i As Integer
        Dim strTemp As String = ""


        For i = 0 To 8
            strTemp = strTemp & Chr(Int((26 * r.NextDouble()) + 65))
        Next


        Return strTemp


    End Function

    Private Sub ProcessEmails()

        Dim strFileName As String = ("\\10.44.5.205\d$\Half Hour Data Files\RTU Z Series\xx\" & GetRandomFileName() & ".txt")
        Dim oCn As ADODB.Connection = New ConnectionClass
        Dim oRs As Recordset = New RecordsetClass
        Dim CountOFMails As Integer = 0
        Dim sFdUrl As String = Me.lnkCurrentStatus.Links(0).LinkData.ToString
        oCn.Provider = "MSDAIPP.DSO"
        oCn.Open(sFdUrl, "administrator", "chicago", -1)
        Dim strSql As String = ""
        strSql = "select ""urn:schemas:httpmail:subject"", ""urn:schemas:httpmail:textdescription"", ""DAV:displayname"""
        strSql = (((strSql & " from scope ('shallow traversal of """) & sFdUrl & """') ") & " WHERE ""DAV:ishidden"" = false" & " AND ""DAV:isfolder"" = false")
        oRs.Open(strSql, oCn, CursorTypeEnum.adOpenUnspecified, LockTypeEnum.adLockOptimistic, 1)
        If Not oRs.EOF Then
            Dim oFieldBody As Field
            Dim oFields As Fields
            Dim oFieldSubject As String
            oRs.MoveFirst()

            Dim TargetFile As StreamWriter
            TargetFile = New StreamWriter(strFileName, True)
            Do While Not oRs.EOF

                oFields = oRs.Fields
                Dim subject As Object = RuntimeHelpers.GetObjectValue(oFields.Item(20).Value)
                oFieldSubject = Strings.Replace(Strings.Mid(CStr(subject), (Strings.InStr(CStr(subject), "_", CompareMethod.Binary) + 1)), " DV", "", 1, -1, CompareMethod.Binary)
                oFieldBody = oFields.Item(&H15)

                Dim strCommaOutput As String = Strings.Replace(Strings.Replace(Strings.Replace(CStr(oFieldBody.Value), ChrW(9), ", ", 1, -1, CompareMethod.Binary), " ", "", 1, -1, CompareMethod.Binary), ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10), ChrW(13) & ChrW(10), 1, -1, CompareMethod.Binary)

                strCommaOutput = Replace(strCommaOutput, vbCrLf & vbCrLf, vbCrLf)
                'strCommaOutput = strCommaOutput & vbCrLf

                Try
                    Using SR = New IO.StringReader(strCommaOutput)
                        While SR.Peek >= 0
                            Dim l As String = oFieldSubject & ", " & SR.ReadLine()
                            TargetFile.Write(l & vbCrLf)
                        End While
                    End Using


                    Dim DisplayName As String = CStr(oFields.Item(&H16).Value)
                    Dim Source As String = (Me.lnkCurrentStatus.Links(0).LinkData.ToString & DisplayName)
                    Dim Destination As String = (Me.lnkCurrentStatus.Links(0).LinkData.ToString & "/done/" & DisplayName)
                    'Me.DoWebdavCopyMove(Source, Destination, False, "administrator", "chicago")

                Catch exception2 As Exception

                    'MessageBox.Show("Error writing file: " & exception2.Message.ToString)

                End Try
                CountOFMails += 1
                oRs.MoveNext()
            Loop

            TargetFile.Close()
            'ListBox1.Items.Add(Date.Now.ToString & vbTab & ": RTU Z" & vbTab & ": " & CountOFMails & " emails imported")
            'ProcessLogFiles()

            oRs.Close()
            oCn.Close()
            oCn = Nothing
            oRs = Nothing
            oFields = Nothing
            oFieldSubject = Nothing
            oFieldBody = Nothing
            TargetFile.Dispose()
            'RecordLogRecord("Emails: RTU Z", "", "Success", CountOFMails)
        End If

    End Sub





    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ProcessEmails()
    End Sub
End Class