Imports HHDataApp.Core
Imports HHDataApp.Utilities.FTP

Imports System.Text.RegularExpressions

Imports System
Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Runtime.CompilerServices
Imports System.Net.Mail
Imports System.Collections.Generic
Imports Microsoft.Exchange.WebServices
Imports Microsoft.Exchange.WebServices.Autodiscover
Imports Microsoft.Exchange.WebServices.Data
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports MSXML2
Imports System.Net
Imports System.Globalization


Imports cca.objects
Imports cca.common
Imports System.Xml

Public Class Form1
    Dim dt As New DataTable 'class level
    Dim dt2 As New DataTable 'class level
    Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
    Private Property oXMLHttp As Object

    Private Sub ProcessRTUZEmails()
        Try

            Dim var_emailaddress As String = My.Settings.RTUz_emailaddress
            Dim var_foldername As String = My.Settings.RTUz_foldername

            Dim exch As New ExchangeService
            Dim xFolderID As FolderId = Nothing
            exch = GetBinding(var_emailaddress)

            Dim strFileName As String = (Core.fnHalfHourDataDirectory & var_foldername & "\" & GetRandomFileName() & ".txt")

            Dim iv As ItemView = New ItemView(2000)
            iv.Traversal = ItemTraversal.Shallow

            Dim filters As SearchFilter.SearchFilterCollection = New SearchFilter.SearchFilterCollection(LogicalOperator.And)
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, False))
            Dim inboxItems As FindItemsResults(Of Item) = Nothing
            inboxItems = exch.FindItems(WellKnownFolderName.Inbox, filters, iv)

            'If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | RTU Z - Emails Start (" & inboxItems.TotalCount & ")") : End If
            If inboxItems.TotalCount > 0 Then
                Dim TargetFile As StreamWriter
                TargetFile = New StreamWriter(strFileName, True)

                ' -- enum items to be deleted --
                For Each i As Item In inboxItems
                    Dim message As EmailMessage = EmailMessage.Bind(exch, i.Id.UniqueId, New PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.Attachments))
                    Dim strCommaOutput As String = Microsoft.VisualBasic.Strings.Replace(Microsoft.VisualBasic.Strings.Replace(Microsoft.VisualBasic.Strings.Replace(CStr(message.Body.Text), ChrW(9), ", ", 1, -1, CompareMethod.Binary), " ", "", 1, -1, CompareMethod.Binary), ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10), ChrW(13) & ChrW(10), 1, -1, CompareMethod.Binary)
                    strCommaOutput = Replace(strCommaOutput, vbCrLf & vbCrLf, vbCrLf)
                    Dim id As String = ""
                    id = Replace(Replace(i.Subject, " DV", ""), "Rtu_", "")

                    Using SR = New IO.StringReader(strCommaOutput)
                        While SR.Peek >= 0

                            Dim l As String = id & ", " & SR.ReadLine()
                            TargetFile.Write(l & vbCrLf)
                        End While
                    End Using

                    Dim view As New FolderView(900)
                    view.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                    view.PropertySet.Add(FolderSchema.DisplayName)
                    ' Return only folders that contain items.
                    Dim searchFilter As SearchFilter = New SearchFilter.IsGreaterThanOrEqualTo(FolderSchema.TotalCount, 0)
                    view.Traversal = FolderTraversal.Deep

                    Dim results As FindFoldersResults = exch.FindFolders(WellKnownFolderName.Inbox, searchFilter, view)
                    Dim folder As Folder

                    For Each folder In results.Folders
                        'Console.WriteLine(folder.DisplayName)
                        If folder.DisplayName = id Then
                            xFolderID = folder.Id
                            Exit For
                        End If
                    Next

                    i.Move(xFolderID)

                    Console.WriteLine(Now & " | RTU Z Email - " + i.Subject + " | " & i.DateTimeReceived.ToString)
                Next

                TargetFile.Dispose()

                'Console.WriteLine(Now & " | RTU Z - Emails End")
            End If

        Catch ex As Exception
            Console.WriteLine(Now & " | RTU Z - " & ex.Message)
        End Try
    End Sub
    Private Sub ProcessRTUZFiles()

        Dim var_emailaddress As String = My.Settings.RTUz_emailaddress
        Dim var_foldername As String = My.Settings.RTUz_foldername


        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & var_foldername & "\")
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.txt")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""
        Dim OutputFile As String = GetRandomFileName() & ".txt"

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | RTU Z - Files Start") : End If

        For Each dra In diar1

            System.IO.File.AppendAllText(dra.Directory.ToString & "\Staging\" & OutputFile, System.IO.File.ReadAllText(dra.Directory.ToString & "\" & dra.ToString))
            If File.Exists(dra.Directory.ToString & "\" & dra.ToString) Then File.Delete(dra.Directory.ToString & "\" & dra.ToString)

        Next

        Dim di2 As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & "RTU Z Cloud\Staging\")
        Dim diar2 As IO.FileInfo() = di2.GetFiles("*.txt")
        Dim dra2 As IO.FileInfo

        For Each dra2 In diar2
            Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportRTUZCSV '" & dra2.Directory.ToString & "\', '" & dra2.ToString & "'"
            strOutput = data_select_value(strSQL, fnConnectionString)

            OutputFromSQL("Files: RTU Z", strOutput, dra2.ToString, dra2.DirectoryName.ToString)
        Next

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | RTU Z - Files End") : End If
    End Sub

    Private Sub ProcessRTU6Emails()

        Try

            ' -- our code will go here ---
            Dim exch As New ExchangeService
            Dim xFolderID As FolderId = Nothing
            exch = GetBinding("logger.rtu6@mcenergygroup.com")
            Dim view As New FolderView(10)
            view.PropertySet = New PropertySet(BasePropertySet.IdOnly)
            view.PropertySet.Add(FolderSchema.DisplayName)
            ' Return only folders that contain items.
            Dim searchFilter As SearchFilter = New SearchFilter.IsGreaterThan(FolderSchema.TotalCount, 0)
            view.Traversal = FolderTraversal.Deep

            Dim results As FindFoldersResults = exch.FindFolders(WellKnownFolderName.Root, searchFilter, view)
            Dim folder As Folder

            For Each folder In results.Folders
                'Console.WriteLine(folder.DisplayName)
                If folder.DisplayName = "Done" Then
                    xFolderID = folder.Id
                End If
            Next

            If results.MoreAvailable Then
                'Make recursive calls with offsets set for the FolderView to get the remaining folders in the result set.
            End If

            If xFolderID Is Nothing Then

                Console.WriteLine("ERROR: Done folder not found!")

            Else

                Dim strFileName As String = (Core.fnHalfHourDataDirectory & "RTU 6 Series\" & GetRandomFileName() & ".txt")


                Dim iv As ItemView = New ItemView(999) '
                'iv.Traversal = ItemTraversal.Shallow '

                ' Dim filters As SearchFilter.SearchFilterCollection = New SearchFilter.SearchFilterCollection(LogicalOperator.And)
                Dim filters As SearchFilter = New SearchFilter.SearchFilterCollection(LogicalOperator.[And], New SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, False), New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, False))


                Dim inboxItems As FindItemsResults(Of Item) = Nothing
                inboxItems = exch.FindItems(WellKnownFolderName.Inbox, filters, iv)

                If inboxItems.Items.Count > 0 Then
                    Dim TargetFile As StreamWriter
                    TargetFile = New StreamWriter(strFileName, True)

                    ' -- enum items to be deleted --
                    For Each i As Item In inboxItems

                        'Console.WriteLine(i.HasAttachments.ToString & " | " & i.Subject)


                        Dim message As EmailMessage = EmailMessage.Bind(exch, i.Id.UniqueId, New PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.Attachments))


                        Dim strCommaOutput As String = Microsoft.VisualBasic.Strings.Replace(Microsoft.VisualBasic.Strings.Replace(Microsoft.VisualBasic.Strings.Replace(CStr(message.Body.Text), ChrW(9), ", ", 1, -1, CompareMethod.Binary), " ", "", 1, -1, CompareMethod.Binary), ChrW(13) & ChrW(10) & ChrW(13) & ChrW(10), ChrW(13) & ChrW(10), 1, -1, CompareMethod.Binary)
                        strCommaOutput = Replace(strCommaOutput, vbCrLf & vbCrLf, vbCrLf)

                        'Console.WriteLine(strCommaOutput)

                        Using SR = New IO.StringReader(strCommaOutput)
                            While SR.Peek >= 0
                                Dim l As String = Replace(Replace(i.Subject, " DV", ""), "Rtu_", "") & ", " & SR.ReadLine()
                                TargetFile.Write(l & vbCrLf)
                            End While
                        End Using

                        message.IsRead = True
                        message.Update(ConflictResolutionMode.AutoResolve)

                        i.Move(xFolderID)
                        Console.WriteLine("Processed - " + i.Subject + " | " & i.DateTimeReceived.ToString)
                        ' i.Delete(DeleteMode.HardDelete)
                    Next

                    TargetFile.Dispose()
                    Console.WriteLine("RTU 6 Done!")
                Else
                    Console.WriteLine("RTU 6 - 0 emails")
                End If

            End If

        Catch ex2 As Exception
            Console.WriteLine("RTU 6 - " & ex2.Message)
        End Try

    End Sub
    Private Sub ProcessRTU6Files()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & "RTU 6 Series\")
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.txt")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""
        Dim OutputFile As String = GetRandomFileName() & ".txt"

        'list the names of all files in the specified directory
        For Each dra In diar1

            System.IO.File.AppendAllText(dra.Directory.ToString & "\Staging\" & OutputFile, System.IO.File.ReadAllText(dra.Directory.ToString & "\" & dra.ToString))
            If File.Exists(dra.Directory.ToString & "\" & dra.ToString) Then File.Delete(dra.Directory.ToString & "\" & dra.ToString)

        Next

        Dim di2 As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & "RTU 6 Series\Staging\")
        Dim diar2 As IO.FileInfo() = di2.GetFiles("*.txt")
        Dim dra2 As IO.FileInfo

        For Each dra2 In diar2
            Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportRTU6CSV '" & dra2.Directory.ToString & "\', '" & dra2.ToString & "'"
            strOutput = data_select_value(strSQL, fnConnectionString)

            OutputFromSQL("Files: RTU 6", strOutput, dra2.ToString, dra2.DirectoryName.ToString)
        Next


    End Sub

    Private Sub ProcessIMServEmails()
        Try
            Dim var_emailaddress As String = My.Settings.IMSERV_emailaddress
            Dim var_foldername As String = My.Settings.IMSERV_foldername

            Dim exch As New ExchangeService
            Dim xFolderID As FolderId = Nothing
            exch = GetBinding(var_emailaddress)

            Dim view As New FolderView(10)
            view.PropertySet = New PropertySet(BasePropertySet.IdOnly)
            view.PropertySet.Add(FolderSchema.DisplayName)

            Dim searchFilter As SearchFilter = New SearchFilter.IsGreaterThanOrEqualTo(FolderSchema.TotalCount, 0)
            view.Traversal = FolderTraversal.Deep

            Dim results As FindFoldersResults = exch.FindFolders(WellKnownFolderName.Inbox, searchFilter, view)
            Dim folder As Folder
            For Each folder In results.Folders
                If folder.DisplayName = "Done" Then
                    xFolderID = folder.Id
                End If
            Next

            Dim iv As ItemView = New ItemView(999) '
            iv.Traversal = ItemTraversal.Shallow '

            Dim filters As SearchFilter.SearchFilterCollection = New SearchFilter.SearchFilterCollection(LogicalOperator.Or)
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, True))
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, False))

            Dim inboxItems As FindItemsResults(Of Item) = Nothing
            inboxItems = exch.FindItems(WellKnownFolderName.Inbox, filters, iv)

            'If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | IMServ - Emails Start") : End If

            For Each i As Item In inboxItems
                Dim message As EmailMessage = EmailMessage.Bind(exch, i.Id.UniqueId, New PropertySet(BasePropertySet.IdOnly, ItemSchema.Attachments))
                If message.Attachments.Count > 0 Then
                    If TypeOf message.Attachments(0) Is FileAttachment Then
                        Dim fileAttachment As FileAttachment = TryCast(message.Attachments(0), FileAttachment)
                        fileAttachment.Load(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\" + fileAttachment.Name)
                    End If
                Else
                    Console.WriteLine(Now & " | IMServ Email - " & i.Subject & " - no attachment")
                End If

                i.Move(xFolderID)
                Console.WriteLine(Now & " | IMServ Email - " + i.Subject)
            Next

            'If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | IMServ - Emails End") : End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
    Private Sub ProcessIMServFiles()

        Dim var_emailaddress As String = My.Settings.IMSERV_emailaddress
        Dim var_foldername As String = My.Settings.IMSERV_foldername

        Dim xFolder As String = Core.fnDataDirectory & "hhdata import\Raw HH Data\" & var_foldername

        ListBox1.Items.Clear()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(xFolder)
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        dt.Clear()
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | IMServ - Files Start") : End If

        For Each dra In diar1
            Dim xFile As String = dra.ToString
            'If dra.Name.Contains("(") Or dra.Name.Contains(")") Then
            '    xFile = GetRandomFileName() & ".csv"
            '    IO.File.Move(dra.FullName, dra.DirectoryName & "\" & xFile)
            'End If

            'File.Copy(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\" & xFile, Core.fnHalfHourDataDirectory & var_foldername & "\" & xFile, True)

            Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportIMServCSV '" & xFolder & "\', '" & xFile & "'"
            strOutput = data_select_value(strSQL, fnConnectionString)

            OutputFromSQL("Files: IMServ Elec", strOutput, xFile, dra.DirectoryName.ToString)

            Console.WriteLine(Now & " | Files: IMServ - " & strOutput)

            If IO.File.Exists(xFolder & "\" & xFile) Then IO.File.Move(xFolder & "\" & xFile, xFolder & "\Loaded\" & xFile)
        Next

        'if diar1.Length > 0 Then : Console.WriteLine(Now & " | IMServ - Files End") : End If
    End Sub


    Public Function RemoveDiacritics(ByVal s As String)
        Dim normalizedString As String
        Dim stringBuilder As New System.Text.StringBuilder
        normalizedString = s.Normalize(System.Text.NormalizationForm.FormD)
        Dim i As Integer
        Dim c As Char
        For i = 0 To normalizedString.Length - 1
            c = normalizedString(i)
            If Globalization.CharUnicodeInfo.GetUnicodeCategory(c) <> Globalization.UnicodeCategory.NonSpacingMark Then
                stringBuilder.Append(c)
            End If
        Next
        Return stringBuilder.ToString()
    End Function

    Private Sub ProcessUKFixedTerminationRequest_Emails()
        Try

            Dim var_emailaddress As String = My.Settings.UKFixedTermination_emailaddress
            Dim var_foldername As String = My.Settings.UKFixedTermination_foldername

            Dim exch As New ExchangeService
            Dim xFolder_Processed As FolderId = Nothing
            Dim xFolder_Error As FolderId = Nothing

            exch = GetBinding(var_emailaddress)

            Dim iv As ItemView = New ItemView(2000)
            iv.Traversal = ItemTraversal.Shallow

            Dim filters As SearchFilter.SearchFilterCollection = New SearchFilter.SearchFilterCollection(LogicalOperator.And)
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, False))

            Dim inboxItems As FindItemsResults(Of Item) = Nothing
            inboxItems = exch.FindItems(WellKnownFolderName.Inbox, filters, iv)

            Dim view As New FolderView(900)
            view.PropertySet = New PropertySet(BasePropertySet.IdOnly)
            view.PropertySet.Add(FolderSchema.DisplayName)

            Dim searchFilter As SearchFilter = New SearchFilter.IsGreaterThanOrEqualTo(FolderSchema.TotalCount, 0)
            view.Traversal = FolderTraversal.Deep

            Dim results As FindFoldersResults = exch.FindFolders(WellKnownFolderName.Inbox, searchFilter, view)
            Dim folder As Folder

            For Each folder In results.Folders
                If folder.DisplayName = "Processed" Then
                    xFolder_Processed = folder.Id
                    Exit For
                End If
            Next

            For Each folder In results.Folders
                If folder.DisplayName = "Error" Then
                    xFolder_Error = folder.Id
                    Exit For
                End If
            Next



            If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | UK.FTR - Start (" & inboxItems.TotalCount & ")") : End If

            If inboxItems.TotalCount > 0 Then
                ' -- enum items to be deleted --
                For Each i As Item In inboxItems

                    Dim message As EmailMessage = EmailMessage.Bind(exch, i.Id.UniqueId, New PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.Attachments))

                    Dim strTitle As String = "Automated UK Fixed Termination Request"
                    Dim strDescription As String = message.Body.Text.ToString
                    Dim strFrom As String = message.From.Address.ToString
                    Dim strRequester As String = RemoveDiacritics(message.From.Name.ToString)

                    Dim intIndex As Integer
                    Try
                        intIndex = strDescription.IndexOf("Please find the enclosed customer account number", 0)
                    Catch ex As Exception

                    End Try

                    If intIndex > 0 Then


                        strDescription = strDescription.Substring(intIndex)

                        Dim reqtemplate As String = "4_Specific_Supplier_Query_Request"
                        Dim operation As String = "AddRequest"

                        Dim strHD As String = fnHelpdeskURL() & fnHelpdeskRESTAPIURL()
                        Dim strXML As String = ""

                        strXML &= "?OPERATION_NAME=ADD_REQUEST&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                        strXML &= "<Operation>"
                        strXML &= "<Details>"

                        strXML &= "<parameter>"
                        strXML &= "<name>requesttemplate</name>"
                        strXML &= "<value>" & reqtemplate & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Requestor</name>"
                        strXML &= "<value>" & strRequester & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>site</name>"
                        strXML &= "<value>Sourcing Team</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>group</name>"
                        strXML &= "<value>UK IRE - Fixed</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Customer</name>"
                        strXML &= "<value>Various</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Subject</name>"
                        strXML &= "<value>" & strTitle & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Description</name>"
                        strXML &= "<value>" & Replace(strDescription, "&", "and") & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "</Details>"
                        strXML &= "</Operation>"

                        Dim outcome As String = PostTo(strHD, strXML)

                        If outcome.StartsWith("Success: ") Then
                            i.Move(xFolder_Processed)
                            message.IsRead = True
                            RecordLogRecord("Emails: UK.FTR", outcome, "Success", 1)
                        Else
                            i.Move(xFolder_Error)
                        End If

                        Console.WriteLine("Processed - " + outcome + " | " & i.DateTimeReceived.ToString)

                    Else
                        i.Move(xFolder_Error)
                        Console.WriteLine("Processed Error - " + message.Subject + " | " & i.DateTimeReceived.ToString)
                    End If
                Next

                Console.WriteLine(Now & " | UK.FTR - End")
            End If

        Catch ex As Exception
            Console.WriteLine(Now & " | UK.FTR - " & ex.Message)
        End Try
    End Sub

    Protected Function PostTo(url As String, postData As String) As String
        Dim myWebRequest As HttpWebRequest = TryCast(WebRequest.Create(url & postData), HttpWebRequest)
        myWebRequest.Method = "POST"
        myWebRequest.ContentType = "application/x-www-form-urlencoded"
        Dim dataStream As System.IO.Stream = myWebRequest.GetRequestStream()
        dataStream.Close()

        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        dataStream = myWebResponse.GetResponseStream()
        Dim reader As New System.IO.StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        Dim xmlDoc As New Xml.XmlDocument
        xmlDoc.LoadXml(responseFromServer)

        Dim formResponse As String = ""
        Try
            Dim outcome As String = xmlDoc.SelectSingleNode("/API/response/operation/result/status").InnerText
            If outcome = "Success" Then
                If postData.Contains("OPERATION_NAME=ADD_REQUEST") Then
                    formResponse = "Success: Request ID " & xmlDoc.SelectSingleNode("/API/response/operation/Details/workorderid").InnerText
                ElseIf postData.Contains("OPERATION_NAME=EDIT_REQUEST") Then
                    formResponse = "Success: " & xmlDoc.SelectSingleNode("/API/response/operation/result/message").InnerText
                ElseIf postData.Contains("OPERATION_NAME=CLOSE_REQUEST") Then
                    formResponse = "Success: "

                End If
            ElseIf outcome = "Failure" Then
                Dim xmlReason As String = xmlDoc.SelectSingleNode("/API/response/operation/result/message").InnerText
                formResponse = "Error: " & xmlReason & "</td></tr>"
            End If
        Catch ex As Exception

        End Try
        reader.Close()
        dataStream.Close()
        myWebResponse.Close()

        Return formResponse

    End Function

    Private Sub ProcessStarkEmails()
        Try

            Dim var_emailaddress As String = My.Settings.Stark_emailaddress
            Dim var_password As String = My.Settings.Stark_password
            Dim var_foldername As String = My.Settings.Stark_foldername

            Dim exch As New ExchangeService
            Dim xFolderID As FolderId = Nothing
            exch = GetBinding(var_emailaddress, var_password)

            Dim view As New FolderView(10)
            view.PropertySet = New PropertySet(BasePropertySet.IdOnly)
            view.PropertySet.Add(FolderSchema.DisplayName)

            Dim searchFilter As SearchFilter = New SearchFilter.IsGreaterThanOrEqualTo(FolderSchema.TotalCount, 0)
            view.Traversal = FolderTraversal.Deep

            Dim results As FindFoldersResults = exch.FindFolders(WellKnownFolderName.Inbox, searchFilter, view)
            Dim folder As Folder
            For Each folder In results.Folders
                If folder.DisplayName = "Done" Then
                    xFolderID = folder.Id
                End If
            Next

            Dim iv As ItemView = New ItemView(999) '
            iv.Traversal = ItemTraversal.Shallow '

            Dim filters As SearchFilter.SearchFilterCollection = New SearchFilter.SearchFilterCollection(LogicalOperator.Or)
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, True))
            filters.Add(New SearchFilter.IsEqualTo(ItemSchema.HasAttachments, False))

            Dim inboxItems As FindItemsResults(Of Item) = Nothing
            inboxItems = exch.FindItems(WellKnownFolderName.Inbox, filters, iv)

            'If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | Stark - Emails Start") : End If

            For Each i As Item In inboxItems
                Dim message As EmailMessage = EmailMessage.Bind(exch, i.Id.UniqueId, New PropertySet(BasePropertySet.IdOnly, ItemSchema.Attachments))
                If message.Attachments.Count > 0 Then
                    If TypeOf message.Attachments(0) Is FileAttachment Then
                        Dim fileAttachment As FileAttachment = TryCast(message.Attachments(0), FileAttachment)
                        fileAttachment.Load(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\" & fileAttachment.Name)
                    End If
                Else
                    Console.WriteLine(Now & " | Stark Email - " & i.Subject & " - no attachment")
                End If

                i.Move(xFolderID)
                Console.WriteLine(Now & " | Stark Email - " + i.Subject)
            Next

            'If inboxItems.TotalCount > 0 Then : Console.WriteLine(Now & " | Stark - Emails End") : End If
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub
    Private Sub ProcessStarkFiles()

        Dim var_foldername As String = My.Settings.Stark_foldername

        ListBox1.Items.Clear()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\")
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        dt.Clear()
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Stark - Files Start") : End If

        For Each dra In diar1
            Dim xFile As String = dra.ToString
            If dra.Name.Contains("(") Or dra.Name.Contains(")") Or dra.Name.Contains(" ") Then
                xFile = dra.Name.Replace("(", "").Replace(")", "").Replace(" ", "")
                IO.File.Move(dra.FullName, dra.DirectoryName & "\" & xFile)
            End If

            File.Copy(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\" & xFile, Core.fnHalfHourDataDirectory & var_foldername & "\" & xFile, True)

            Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportStarkCSV 'd:\Half Hour Data Files\" & var_foldername & "\', '" & xFile & "'"
            strOutput = data_select_value(strSQL, fnConnectionString)

            OutputFromSQL("Files: Stark Elec", strOutput, xFile, dra.DirectoryName.ToString)

            If IO.File.Exists(Core.fnHalfHourDataDirectory & var_foldername & "\" & xFile) Then IO.File.Delete(Core.fnHalfHourDataDirectory & var_foldername & "\" & xFile)
        Next

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Stark - Files End") : End If
    End Sub


    Private Sub ProcessPhillipsLightingFiles()

        Dim var_foldername As String = My.Settings.Philips_foldername

        ListBox1.Items.Clear()
        Dim di As New IO.DirectoryInfo(Core.fnTyrrellDataDirectory & "MEN Arena\" & var_foldername & "\")

        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Phillips - Files Start") : End If
        For Each dra In diar1
            If File.Exists(Core.fnHalfHourDataDirectory & "Philips Lighting\" & dra.ToString) Then File.Delete(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)

            IO.File.Copy(dra.Directory.ToString & "\" & dra.ToString, Core.fnHalfHourDataDirectory & "Philips Lighting\" & dra.ToString)
            strOutput = data_select_value("EXEC UML_EXTData.dbo.usp_HHDataImport_ImportTyrellCSV 'd:\Half Hour Data Files\" & var_foldername & "\', '" & dra.ToString & "'", fnConnectionString)

            OutputFromSQL("Files: Philips Lighting", strOutput, dra.ToString, dra.DirectoryName.ToString)
        Next
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Phillips - Files End") : End If
    End Sub

    Private Sub ProcessTrendBMSFiles()

        Dim var_foldername As String = My.Settings.Trend_foldername

        ListBox1.Items.Clear()

        Dim di As New IO.DirectoryInfo(Core.fnTyrrellDataDirectory & "MEN Arena\" & var_foldername & "\")

        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        'list the names of all files in the specified directory

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Trend - Files Start") : End If
        For Each dra In diar1

            If File.Exists(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString) Then File.Delete(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)
            IO.File.Copy(dra.Directory.ToString & "\" & dra.ToString, Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)

            strOutput = data_select_value("EXEC UML_EXTData.dbo.usp_HHDataImport_ImportTyrellCSV 'd:\Half Hour Data Files\" & var_foldername & "\', '" & dra.ToString & "'", fnConnectionString)

            OutputFromSQL("Files: Trend BMS", strOutput, dra.ToString, dra.DirectoryName.ToString)

        Next

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Trend - Files End") : End If
    End Sub

    Private Sub ProcessGenericFiles()

        Dim var_foldername As String = My.Settings.Generic_foldername

        ListBox1.Items.Clear()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnDataDirectory & "hhdata import\Raw HH Data\" & var_foldername)
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv")
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Generic - Files Start") : End If
        'list the names of all files in the specified directory
        For Each dra In diar1

            If IsFileOpen(dra.Directory.ToString & "\" & dra.ToString) = False Then

                'If IO.File.Exists(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString) Then
                '    IO.File.AppendAllText(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString, System.IO.File.ReadAllText(dra.Directory.ToString & "\" & dra.ToString))
                'Else
                '    IO.File.Copy(dra.Directory.ToString & "\" & dra.ToString, Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)
                'End If

                Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportGenericCSV '" & dra.Directory.ToString & "\', '" & dra.ToString & "'"
                strOutput = data_select_value(strSQL, fnConnectionString)

                OutputFromSQL("Files: Generic Supplier", strOutput, dra.ToString, dra.DirectoryName.ToString)
            Else
                Dim strError As String = "In file: " & dra.ToString & vbCrLf & "Error Details: " & "The file is already open.  The system will try and import this file on the next pass."
                SendSMTP("donotreply@ems.schneider-electric.com", "hhdataimport@utilitymasters.co.uk", "HH Data Generic Import Error", strError, "")
            End If

        Next
6:
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Generic - Files End") : End If
    End Sub

    Private Sub ProcessCarillionaMTRecords()

        Dim var_foldername As String = My.Settings.Carillion_foldername

        ListBox1.Items.Clear()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnDataDirectory & "\hhdata import\Raw HH Data\" & var_foldername & "\") ' source
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv") 'file type
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        'list the names of all files in the specified directory
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Carillion - Files Start") : End If
        For Each dra In diar1

            If IsFileOpen(dra.Directory.ToString & "\" & dra.ToString) = False Then

                If IO.File.Exists(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString) Then
                    IO.File.AppendAllText(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString, System.IO.File.ReadAllText(dra.Directory.ToString & "\" & dra.ToString))
                Else
                    IO.File.Copy(dra.Directory.ToString & "\" & dra.ToString, Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)
                End If

                Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportCarillionCSV '" & "d:\Half Hour Data Files\" & var_foldername & "\', '" & dra.ToString & "'"
                strOutput = data_select_value(strSQL, fnConnectionString)

                OutputFromSQL("Files: Carillion aMT", strOutput, dra.ToString, dra.DirectoryName.ToString)

                If IO.File.Exists(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString) Then IO.File.Delete(Core.fnHalfHourDataDirectory & var_foldername & "\" & dra.ToString)

            Else
                Dim strError As String = "In file: " & dra.ToString & vbCrLf & "Error Details: " & "The file is already open.  The system will try and import this file on the next pass."
                SendSMTP("donotreply@ems.schneider-electric.com", "hhdataimport@utilitymasters.co.uk", "HH Data Generic Import Error", strError, "")
            End If

        Next

        ' If diar1.Length > 0 Then : Console.WriteLine(Now & " | Carillion - Files End") : End If

    End Sub

    Private Sub ProcessFTPGazpromElecFiles()

        Dim host As String = My.Settings.FTP_gazprom_url
        Dim username As String = My.Settings.FTP_gazprom_username
        Dim password As String = My.Settings.FTP_gazprom_password
        Dim stLocalDir As String = Core.fnHalfHourDataDirectory & "Gazprom\"

        'Create a link to an FtpServer
        Dim ftp As New clsFTPClient(host, username, password)

        Dim strDate As String = ""

        'Get the detailed directory listing of /pub/
        Dim dirList As FTPdirectory = ftp.ListDirectoryDetail("/")

        'filter out only the files in the list
        Dim filesOnly As FTPdirectory = dirList.GetFiles()
        'download these files
        Dim x As Integer = 0
        If filesOnly.Count > 0 Then : Console.WriteLine(Now & " | Gazprom - FTP Start") : x = 1 : End If

        For Each file As FTPfileInfo In filesOnly
            strDate = Replace(Replace(Replace(Date.Now(), "/", ""), " ", "_"), ":", "")
            'Console.WriteLine(file.Filename)

            Dim strFileName As String = (strDate & "_" & GetRandomFileName() & ".csv")

            ftp.Download(file, stLocalDir & strFileName, True)
            ftp.FtpRename(file.Filename, "processed/" & strFileName)
            RecordLogRecord("FTP: Gazprom Elec", stLocalDir & strFileName, "Success", 1)
            'RecordLogRecord()
        Next
        If x = 1 Then : Console.WriteLine(Now & " | Gazprom - FTP End") : x = 0 : End If


    End Sub
    Private Sub ProcessFTPGazpromElecRecords()
        Dim strFileSize As String = ""
        Dim di As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & "Gazprom")
        Dim aryFi As IO.FileInfo() = di.GetFiles("*.csv")
        Dim fi As IO.FileInfo

        If aryFi.Length > 0 Then : Console.WriteLine(Now & " | Gazprom - Files Start") : End If
        For Each fi In aryFi
            Dim strSQLOutput As String = Core.data_select_value("EXEC UML_Extdata.dbo.usp_HHDataImport_ImportGazpromElec '" & fi.FullName & "'", fnConnectionString)
            OutputFromSQL("Files: Gazprom Elec", strSQLOutput, fi.Name, fi.Directory.FullName)
        Next
        If aryFi.Length > 0 Then : Console.WriteLine(Now & " | Gazprom - Files End") : End If
    End Sub

    Private Sub ProcessFTPTruReadGasFiles()
        Dim host As String = My.Settings.FTP_truread_url
        Dim username As String = My.Settings.FTP_truread_username
        Dim password As String = My.Settings.FTP_truread_password
        Dim stLocalDir As String = Core.fnHalfHourDataDirectory & "TruRead\"

        'Create a link to an FtpServer
        Dim ftp As New clsFTPClient(host, username, password)
        Dim strDate As String = ""
        'get a simple listing of the root directory /
        ftp.CurrentDirectory = "/"
        Dim result As List(Of String) = ftp.ListDirectory("/out/")
        ''display in console
        'Console.WriteLine("Directory listing of " & ftp.CurrentDirectory)
        Dim x As Integer = 0
        ' If result.Count > 0 Then : Console.WriteLine(Now & " | TruRead - FTP Start") : x = 1 : End If
        If result.Count > 0 Then : x = 1 : End If

        For Each line As String In result
            If line > "" Then

                'Console.WriteLine("/out/" & line)
                strDate = Replace(Replace(Replace(Date.Now(), "/", ""), " ", "_"), ":", "")
                Dim strFileName As String = (strDate & "_" & GetRandomFileName() & ".csv")

                ftp.Download("/out/" & line, stLocalDir & strFileName, True)

                If File.Exists(stLocalDir & strFileName) Then
                    ftp.FtpDelete("/out/" & line)
                    ftp.Upload(stLocalDir & strFileName, "/processed/" & strFileName)
                End If
                RecordLogRecord("TruRead FTP File", stLocalDir & strFileName, "Success", 1)

            End If
        Next

        'If x = 1 Then : Console.WriteLine(Now & " | TruRead - FTP End") : x = 0 : End If
        If x = 1 Then : x = 0 : End If

    End Sub
    Private Sub ProcessFTPTruReadGasRecords()
        Dim strFileSize As String = ""
        Dim di As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & "TruRead\")
        Dim aryFi As IO.FileInfo() = di.GetFiles("*.csv")
        Dim fi As IO.FileInfo
        'If aryFi.Length > 0 Then : Console.WriteLine(Now & " | TruRead - Files Start") : End If
        For Each fi In aryFi
            Dim strSQLOutput As String = Core.data_select_value("EXEC UML_Extdata.dbo.usp_HHDataImport_ImportTruReadGas '" & fi.FullName & "'", fnConnectionString)
            OutputFromSQL("Files: TruRead Gas", strSQLOutput, fi.Name, fi.Directory.FullName)
        Next
        'If aryFi.Length > 0 Then : Console.WriteLine(Now & " | TruRead - Files End") : End If
    End Sub

    ' General Functions
    Public Property CommandTimeout() As Integer
        Get

        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Public Shared Function GetBinding(ByVal EmailAddress As String, Optional ByVal password As String = "Pa$$w0rd") As ExchangeService
        ' Create the binding.
        Dim service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)

        ' Define credentials.
        service.Credentials = New WebCredentials(EmailAddress, password)
        Try
            Select Case EmailAddress
                Case "logger.rtuz@mcenergygroup.com"
                    service.Url = New Uri("https://ch1prd0810.outlook.com/EWS/Exchange.asmx")
                Case "logger.rtuz@mcenergygroup.com"
                    service.Url = New Uri("https://by2prd0810.outlook.com/EWS/Exchange.asmx")
                Case "hhdata.imserv@mcenergygroup.com"
                    service.Url = New Uri("https://bluprd0811.outlook.com/EWS/Exchange.asmx")
                Case "logger.rtuz@ems.schneider-electric.com"
                    service.Url = New Uri("https://bl2prd0810.outlook.com/EWS/Exchange.asmx")
                Case "hhdata.imserv@ems.schneider-electric.com"
                    service.Url = New Uri("https://bluprd0811.outlook.com/EWS/Exchange.asmx")
                Case "hhdata_stark@ems.schneider-electric.com"
                    service.Url = New Uri("https://bl2prd0810.outlook.com/EWS/Exchange.asmx")
                Case "UK.Fixed.Termination.Request@ems.schneider-electric.com"
                    service.Url = New Uri("https://ch1prd0810.outlook.com/EWS/Exchange.asmx")
                Case Else
                    service.AutodiscoverUrl(EmailAddress, AddressOf RedirectionUrlValidationCallback)
                    Console.WriteLine("AutodiscoverURL: " & service.Url.ToString())
            End Select
        Catch ex1 As Exception
            Try
                ' Use the AutodiscoverUrl method to locate the service endpoint.
                service.AutodiscoverUrl(EmailAddress, AddressOf RedirectionUrlValidationCallback)
            Catch ex As AutodiscoverRemoteException
                Console.WriteLine(("Exception thrown: " & ex.Error.Message))
            End Try
        End Try
        ' Display the service URL.
        'Console.WriteLine("AutodiscoverURL: " & service.Url.ToString())
        Return service
    End Function
    Public Shared Function RedirectionUrlValidationCallback(ByVal redirectionUrl As String) As Boolean
        ' Perform validation.
        Return (redirectionUrl = "https://autodiscover-s.outlook.com/autodiscover/autodiscover.xml")
    End Function
    Public Shared Function GetAttachmentsListXML(ByVal sHREF As String, ByVal sUserName As String, ByVal sPassword As String) As String

        Dim HttpWebRequest As MSXML2.XMLHTTP30
        Dim strPropReq As String = Nothing
        Dim strOutPutFile As String = Nothing

        HttpWebRequest = New MSXML2.XMLHTTP30

        With HttpWebRequest
            .open("X-MS-ENUMATTS", sHREF, False, sUserName, sPassword)
            .setRequestHeader("Content-type:", "text/xml")
            .setRequestHeader("Depth", "1,noroot")
            .send()

            GetAttachmentsListXML = HttpWebRequest.responseText
        End With

        HttpWebRequest = Nothing
    End Function
    Public Shared Function ReadAnAttatchment(ByVal sHREF As String, ByVal sUserName As String, ByVal sPassword As String) As Object

        Dim HttpWebRequest As MSXML2.XMLHTTP30

        HttpWebRequest = New MSXML2.XMLHTTP30
        HttpWebRequest.open("GET", sHREF, False, sUserName, sPassword)

        HttpWebRequest.send()

        ReadAnAttatchment = HttpWebRequest.responseText ' Returns as text
        'ReadAnAttatchment = HttpWebRequest.responseBody ' Returns as encoded
        Return ReadAnAttatchment

        HttpWebRequest = Nothing
    End Function
    Public Shared Function GetRandomFileName() As String


        Dim r As New Random()
        Dim i As Integer
        Dim strTemp As String = ""


        For i = 0 To 8
            strTemp = strTemp & Chr(Int((26 * r.NextDouble()) + 65))
        Next


        Return strTemp


    End Function
    Public Shared Function IsFileOpen(ByVal filename As String) As Boolean
        Using fs As FileStream = File.Open(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.None)
            fs.Close()
            Return False
        End Using
        Return True

    End Function
    Public Shared Function GetDataTableFromArray(ByVal array As Object()) As DataTable

        Dim dataTable As New DataTable()

        dataTable.LoadDataRow(array, True) 'Pass array object to LoadDataRow method 

        Return dataTable

    End Function

    Private Sub DownloadFile(ByVal uri As String, ByVal destFile As String, Optional ByVal username As String = Nothing, Optional ByVal pwd As String = Nothing)
        Dim wc As New System.Net.WebClient
        If Not username Is Nothing AndAlso Not pwd Is Nothing Then
            wc.Credentials = New System.Net.NetworkCredential(username, pwd)
        End If
        wc.DownloadFile(uri, destFile)
    End Sub
    Private Sub SendSMTP(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String, Optional ByVal format As String = "Text", Optional ByVal BCC As String = "", Optional ByVal User As String = "", Optional ByVal Pass As String = "")
        Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
        With insMail
            .Subject = strSubject
            If format = "HTML" Then : .IsBodyHtml = True : Else : .IsBodyHtml = True : End If
            .Body = strBody
            If BCC.Length > 0 Then
                .Bcc.Add(New MailAddress(BCC))
            End If
            .Bcc.Add(New MailAddress("dave.clarke@ems.schneider-electric.com"))
            If Not strAttachments.Equals(String.Empty) Then
                Dim strFile As String
                Dim strAttach() As String = strAttachments.Split(";"c)
                For Each strFile In strAttach
                    .Attachments.Add(New System.Net.Mail.Attachment(strFile.Trim()))
                Next
            End If
        End With
        Dim smtp As New System.Net.Mail.SmtpClient

        If User.Length > 0 Then
            smtp.Credentials = New NetworkCredential(User, Pass)
        Else
            smtp.Credentials = CredentialCache.DefaultNetworkCredentials
        End If

        smtp.Host = fnSMTPServer()
        smtp.Port = 25
        Try

            smtp.Send(insMail)
        Catch ex As Exception
            MsgBox("ERROR!! Sending Email to Terminations: " & ex.Message)
        End Try
    End Sub

    Private Sub OutputFromSQL(ByVal strType As String, ByVal strOutput As String, ByVal strFileName As String, ByVal strDirectory As String)
        'Console.WriteLine(Now & " | " & strFileName & " | " & strOutput)
        Dim nicedate As String = Replace(Now.Date.Date, "/", "")
        If Not strOutput.StartsWith("Success") Then
            Dim user As String = System.IO.File.GetAccessControl(strDirectory & "\" & strFileName).GetOwner(GetType(System.Security.Principal.NTAccount)).ToString()
            Dim strError As String = "<b>File :</b> " & strFileName & "<br />"
            strError &= "<b>User :</b> " & user & "<br />"
            strError &= "<b>Details :</b> " & strOutput & "<br /><br />"
            strError &= "This file has been attached to this email, please fix and re-save to the following directory: " & "<br />"
            strError &= "     " & strDirectory & "\"
            SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@mcenergygroup.com", "Interval Data Load Application " & strType & " Import Error", strError, strDirectory & "\" & strFileName, "HTML")
            If IO.File.Exists(strDirectory & "\" & strFileName) Then IO.File.Delete(strDirectory & "\" & strFileName)
        Else
            If strType = "Files: Trend BMS" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Tyrrell\Trend BMS Loaded\" & strFileName, System.IO.File.ReadAllText(Core.fnHalfHourDataDirectory & "Trend BMS\" & strFileName))
                If File.Exists(Core.fnHalfHourDataDirectory & "Trend BMS\" & strFileName) Then File.Delete(Core.fnHalfHourDataDirectory & "Trend BMS\" & strFileName)

            ElseIf strType = "Files: Philips Lighting" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Tyrrell\Philips Lighting Loaded\" & strFileName, System.IO.File.ReadAllText(Core.fnHalfHourDataDirectory & "Philips Lighting\" & strFileName))
                If File.Exists(Core.fnHalfHourDataDirectory & "Philips Lighting\" & strFileName) Then File.Delete(Core.fnHalfHourDataDirectory & "Philips Lighting\" & strFileName)

            ElseIf strType = "Files: RTU Z" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\RTU Z Cloud\Loaded\" & nicedate & ".txt", System.IO.File.ReadAllText(Core.fnHalfHourDataDirectory & "RTU Z Cloud\Staging\" & strFileName))
                If File.Exists(Core.fnHalfHourDataDirectory & "RTU Z Cloud\Staging\" & strFileName) Then File.Delete(Core.fnHalfHourDataDirectory & "RTU Z Cloud\Staging\" & strFileName)

            ElseIf strType = "Files: RTU 6" Then


                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\RTU 6 Series\Loaded\" & nicedate & ".txt", System.IO.File.ReadAllText(Core.fnHalfHourDataDirectory & "RTU 6 Series\Staging\" & strFileName))
                If File.Exists(Core.fnHalfHourDataDirectory & "RTU 6 Series\Staging\" & strFileName) Then File.Delete(Core.fnHalfHourDataDirectory & "RTU 6 Series\Staging\" & strFileName)

            ElseIf strType = "Files: Gazprom Elec" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Gazprom\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: TruRead Gas" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\TruRead\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: IMServ Elec" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\IMServ Cloud\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: Stark Elec" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Stark Cloud\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: Generic Supplier" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\- Generic\Loaded\" & nicedate & "_" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: Carillion aMT" Then

                System.IO.File.AppendAllText(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Carillion aMT\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))

            ElseIf strType = "Files: Carillion FTP" Then

                If IO.File.Exists(strDirectory & "\Loaded\" & strFileName) Then
                    IO.File.AppendAllText(strDirectory & "\Loaded\" & strFileName, System.IO.File.ReadAllText(strDirectory & "\" & strFileName))
                Else
                    IO.File.Copy(strDirectory & "\" & strFileName, strDirectory & "\Loaded\" & strFileName)
                End If

            Else

                System.IO.File.AppendAllText(strDirectory & "\Loaded\" & Replace(Replace(strFileName, ".csv", ""), ".CSV", "") & "_" & Replace(Replace(Replace(Date.Now.ToString, "/", ""), ":", ""), " ", "_") & ".csv", System.IO.File.ReadAllText(Core.fnHalfHourDataDirectory & "" & strType & "\" & strFileName))

            End If

            If File.Exists(strDirectory & "\" & strFileName) Then File.Delete(strDirectory & "\" & strFileName)
            RecordLogRecord(strType, strFileName, "Success", CInt(Replace(Replace(Replace(strOutput, "Success: ", ""), "File Imported (", ""), " new records)", "")))


            'SendSMTP("intervaldataloader@mcenergygroup.com", "dave.clarke@mcenergygroup.com", "Interval Data Loaded", strOutput, "")


        End If


    End Sub

    Private Sub subExportDGVToCSV(ByVal strExportFileName As String, _
ByVal DataGridView As DataGridView, _
Optional ByVal blnWriteColumnHeaderNames As Boolean = False, _
Optional ByVal strDelimiterType As String = ",")

        '* Create a StreamWriter object to open and write contents
        '* of the DataGridView columns and rows.
        Dim sr As StreamWriter = File.CreateText(strExportFileName)

        Dim strDelimiter As String = strDelimiterType

        '* Create a variable that holds the total number of columns
        '* in the DataGridView.
        Dim intColumnCount As Integer = DataGridView.Columns.Count - 1

        '* Create a variable to hold the row data
        Dim strRowData As String = ""

        '* Now collect data for each row and write to the CSV file.
        '* Loop through each row in the DataGridView.
        For intX As Integer = 0 To DataGridView.Rows.Count - 2

            '* Reset the value of the strRowData variable
            strRowData = ""

            For intRowData As Integer = 0 To intColumnCount

                '* The IIf statement will not put a delimiter after the 
                '* last value added.

                '* The Replace function will remove the delimiter
                '* from the field data if found.

                Dim CellValue As String = DataGridView.Rows(intX).Cells(intRowData).Value.ToString

                CellValue = IIf(CellValue = Nothing, "_", CellValue)

                strRowData += Replace(CellValue, strDelimiter, "") & _
                    IIf(intRowData < intColumnCount, strDelimiter, "")

            Next intRowData

            '* Write the row data to the CSV file.
            sr.WriteLine(strRowData)

        Next intX

        '* Close the StreamWriter object.
        sr.Close()

        '* You are done!
        'MsgBox("Done!")

    End Sub
    Private Sub DoWebdavCopyMove(ByVal sSourceURL As String, ByVal sDestinationURL As String, ByVal bCopy As Boolean, ByVal sUser As String, ByVal sPassword As String)
        Dim oXMLHttp = CreateObject("microsoft.xmlhttp") ' = New MSXML2.XMLHTTP30
        Dim sVerb As String

        If bCopy Then
            sVerb = "COPY"
        Else
            sVerb = "MOVE"
        End If

        If sUser <> "" Then
            oXMLHttp.Open(sVerb, sSourceURL, False, sUser, sPassword)   ' TODO: Change username and password
        Else
            oXMLHttp.Open(sVerb, sSourceURL, False, "uml\administrator", "chicago") ', sUser, sPassword   ' TODO: Change username and password
        End If

        oXMLHttp.setRequestHeader("Destination", sDestinationURL)

        ' Send the stream across
        oXMLHttp.Send()

        If (oXMLHttp.Status >= 200 And oXMLHttp.Status < 300) Then
            ListBox1.Items.Add("Success!   " & "Results = " & oXMLHttp.Status & ": " & oXMLHttp.statusText)
        ElseIf oXMLHttp.Status = 401 Then
            ListBox1.Items.Add("You don't have permission to do the job!")
        Else
            ListBox1.Items.Add("Request Failed.  Result! = " & oXMLHttp.Status & ": " & oXMLHttp.statusText)
        End If

        oXMLHttp = Nothing

    End Sub
    Private Sub RecordLogRecord(ByVal strApplicationName As String, ByVal strFileName As String, ByVal strOutcome As String, ByVal intRecordsImported As Integer)

        Dim varSQL As String

        varSQL = "INSERT INTO UML_ExtData.dbo.tblApp_DataLoad_Logs (varApplicationName, varFileName, varOutcome, intRecordsImported) "
        varSQL &= "VALUES ('" & strApplicationName & "', '" & strFileName & "', '" & strOutcome & "', " & intRecordsImported & ") "

        'Core.data_execute_nonquery(varSQL, fnConnectionString)

    End Sub
    Private Sub ProcessLogFiles()

        Using SW As New IO.StreamWriter(Core.fnDataDirectory & "\hhdata import\Raw HH Data\Log.txt", True)
            For Each itm As String In Me.ListBox1.Items
                SW.WriteLine(itm)
            Next
        End Using

        ListBox2.Items.Clear()

        Dim file_name As String = Core.fnDataDirectory & "\hhdata import\Raw HH Data\Log.txt"
        Dim stream_reader As New IO.StreamReader(file_name)
        Dim line As String

        ' Read the file one line at a time.
        line = stream_reader.ReadLine()
        Do While Not (line Is Nothing)
            ' Trim and make sure the line isn't blank.
            line = line.Trim()
            If line.Length > 0 Then _
                ListBox2.Items.Add(line)

            ' Get the next line.
            line = stream_reader.ReadLine()
        Loop
        ListBox2.SelectedIndex = 0
        stream_reader.Close()


        Me.ListBox2.SelectedIndex = ListBox2.Items.Count - 1

    End Sub

    ' Form elements
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            lblSummary.Text = Now & " | " & System.Environment.UserName
            LoadInfoBox_Today()

            'Dim rq = System.Net.WebRequest.Create(New Uri("https://www.gov.uk/government/publications?keywords=&publication_filter_option=consultations&topics%5B%5D=energy&departments%5B%5D=all&official_document_status=all&world_locations%5B%5D=all&from_date=01%2F08%2F2016&to_date="))
            'Try
            '    Dim myWebResponse As WebResponse = rq.GetResponse
            '    Dim ReceiveStream As Stream = myWebResponse.GetResponseStream()
            '    Dim encode As System.Text.Encoding = System.Text.Encoding.GetEncoding("utf-8")
            '    Dim readStream As New StreamReader(ReceiveStream, encode)
            '    Console.WriteLine(ControlChars.Cr + "Response stream received")
            '    Dim str As String = readStream.ReadToEnd()
            '    Dim cut_at As String = "filter-results js-filter-results"
            '    Dim x As Integer = InStr(str, cut_at)
            '    str = str.Substring(x + cut_at.Length + 31)

            '    cut_at = "report-a-problem-container"
            '    x = InStr(str, cut_at)
            '    str = str.Substring(0, x - 91)

            '    ' Remove Doctype ( HTML 5 )
            '    str = Regex.Replace(str, "<!(.|\s)*?>", "")

            '    ' Remove HTML Tags
            '    str = Regex.Replace(str, "</?[b-z][b-z0-9]*[^<>]*>", "")

            '    ' Remove HTML Comments
            '    str = Regex.Replace(str, "<!--(.|\s)*?-->", "")

            '    ' Remove Script Tags
            '    str = Regex.Replace(str, "<script.*?</script>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            '    ' Remove Stylesheets
            '    str = Regex.Replace(str, "<style.*?</style>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            '    Dim items() As String = str.Split("<a href")

            '    Console.WriteLine("")

            '    ' Release the resources of stream object.
            '    readStream.Close()

            '    ' Release the resources of response object.
            '    myWebResponse.Close()


            'Catch Ex As System.Net.WebException
            '    Dim rs = Ex.Response
            '    'Call (New StreamReader(rs.GetResponseStream)).ReadToEnd.Dump()
            'End Try

        Catch ex As Exception

        End Try

    End Sub
    Private Sub time_Progress_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles time_Progress.Tick
        Try

            If Me.prgTimer.Value = 0 Then


                '
                If Me.chkFTPCarillion.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Carillion FTP..."
                    Me.Refresh()
                    Try
                        ProcessFTPCarillionFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Carillion FTP | " & ex.Message)
                    End Try

                End If

                If Me.chkFilesCarillionFTP.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Carillion FTP Files..."
                    Me.Refresh()
                    Try
                        ProcessFTPCarillionRecords()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Carillion FTP Files | " & ex.Message)
                    End Try

                End If

                If Me.chkSEMOXMLFiles.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | SEMO Pricing..."
                    Me.Refresh()
                    Try
                        CheckSEMOPricing()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | SEMO Pricing | " & ex.Message)
                    End Try

                End If

                If Me.chkBudgetRequests_MultiCountry.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Multi Country Budgets..."
                    Me.Refresh()
                    Try
                        Process_BudgetRequests_MultiCountry()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Multi Country Budgets | " & ex.Message)
                    End Try

                End If

                If Me.chkBudgetRequests_MultiClient.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Multi Client Budgets..."
                    Me.Refresh()
                    Try
                        Process_BudgetRequests_MultiClient()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Multi Client Budgets | " & ex.Message)
                    End Try

                End If


                If Me.chkUKFixedTerminationRequest.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | UK Fixed Termination Requests..."
                    Me.Refresh()
                    Try
                        ProcessUKFixedTerminationRequest_Emails()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | UK.FTR | " & ex.Message)
                    End Try

                End If

                If Me.chkDataServices.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Data Services..."
                    Me.Refresh()
                    Try
                        ProcessDataServices()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Data Services | " & ex.Message)
                    End Try
                End If

                If Me.chkAutomatedReports.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Automated Reports..."
                    Me.Refresh()
                    Try
                        ProcessAutomatedReports()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Automated Reports | " & ex.Message)
                    End Try
                End If


                If Me.chkTerminations.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Terminations..."
                    Me.Refresh()
                    Try
                        ProcessTerminations()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Terminations | " & ex.Message)
                    End Try
                End If

                If Me.chkTenantStatments.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Tenant Statements..."
                    Me.Refresh()
                    Try
                        ProcessTenantStatements()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Tenant Statements | " & ex.Message)
                    End Try
                End If

                If Me.chkEmailStark.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Stark Emails..."
                    Me.Refresh()
                    Try
                        ProcessStarkEmails()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Stark Emails | " & ex.Message)
                    End Try
                End If

                If Me.chkFilesStark.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Stark Files..."
                    Me.Refresh()
                    Try
                        ProcessStarkFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Stark Files | " & ex.Message)
                    End Try
                End If


                If Me.chkRTUZ.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | RTU Z Emails..."
                    Me.Refresh()
                    ProcessRTUZEmails()
                End If

                If Me.chkRTUZFiles.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | RTU Z Files..."
                    Me.Refresh()
                    Try
                        ProcessRTUZFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | RTU Z Files | " & ex.Message)
                    End Try
                End If

                If Me.chkIMServEmails.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | IMServ Emails..."
                    Me.Refresh()
                    ProcessIMServEmails()
                End If

                If Me.chkIMServ.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | IMServ Files..."
                    Me.Refresh()
                    Try
                        ProcessIMServFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | IMServ Files | " & ex.Message)
                    End Try
                End If

                If Me.chkFTPTruReadElecFiles.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | FTP: TruRead Gas ..."
                    Me.Refresh()
                    Try
                        ProcessFTPTruReadGasFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | TruRead FTP | " & ex.Message)
                    End Try
                End If

                If Me.chkFTPTruReadElecRecords.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | File: TruRead Gas ..."
                    Me.Refresh()
                    Try
                        ProcessFTPTruReadGasRecords()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | TruRead Files | " & ex.Message)
                    End Try
                End If

                If Me.chkFTPGazpromElecFiles.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | FTP: Gazprom Elec ..."
                    Me.Refresh()
                    Try
                        ProcessFTPGazpromElecFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Gazprom FTP | " & ex.Message)
                    End Try
                End If

                If Me.chkFTPGazpromElecRecords.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | File: Gazprom Elec ..."
                    Me.Refresh()
                    Try
                        ProcessFTPGazpromElecRecords()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Gazprom Files | " & ex.Message)
                    End Try
                End If

                If Me.chkTrend.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Trend BMS Files..."
                    Me.Refresh()
                    Try
                        ProcessTrendBMSFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Trend BMS | " & ex.Message)
                    End Try
                End If

                If Me.chkPhillips.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Phillips Lighting Files..."
                    Me.Refresh()
                    Try
                        ProcessPhillipsLightingFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Phillips | " & ex.Message)
                    End Try
                End If

                If Me.chkGeneric.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | Supplier Files..."
                    Me.Refresh()
                    Try
                        ProcessGenericFiles()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Generic Files | " & ex.Message)
                        SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Interval Data Load Application Exception Error", ex.Message, "", "Text")
                    End Try
                End If

                If Me.chkCarillionaMT.Checked Then
                    Me.ToolStripStatusLabel1.Text = Now() & " | File: Carillion aM&T ..."
                    Me.Refresh()
                    Try
                        ProcessCarillionaMTRecords()
                    Catch ex As Exception
                        Console.WriteLine(Now & " | Carillion Files | " & ex.Message)
                    End Try
                End If

                LoadInfoBox_Today()
                'MissingSubMeterReport()

            End If

            If Me.prgTimer.Value = Me.prgTimer.Maximum Then
                Me.prgTimer.Value = 0
                Me.ToolStripStatusLabel1.Text = Now()
            Else
                Me.prgTimer.Value += 1
                Me.ToolStripStatusLabel1.Text = Now()
            End If
        Catch ex As Exception
            If Not ex.Message.ToString.StartsWith("Timeout expired.") Then : Console.WriteLine(Date.Now.ToString & vbTab & ": Error" & vbTab & ": " & ex.Message.ToString & " !!") : End If
        End Try

        Me.lblCountdown.Text = (Me.prgTimer.Maximum - Me.prgTimer.Value) & " seconds remaining."

        'LoadInfoBox_Today()

        Me.Refresh()

    End Sub
    Private Sub btnProcessSwitch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessSwitch.Click
        If Me.time_Progress.Enabled = False Then
            Me.btnProcessSwitch.Text = "Stop Processing"
            Me.time_Progress.Enabled = True
            Me.prgTimer.Maximum = CInt(Me.txtTimerInterval.Text) * 60
            Me.prgTimer.Value = 0
            Me.txtTimerInterval.Enabled = False
        Else
            Me.time_Progress.Enabled = False
            Me.btnProcessSwitch.Text = "Start Processing"
            Me.txtTimerInterval.Enabled = True
            Me.lblCountdown.Text = Nothing
        End If

        '' Create a service binding and reuse it for each EWS call.
        'Dim service = New ExchangeService
        'service = GetBinding()

        '' Pause the console.
        'Console.WriteLine(ChrW(10) & ChrW(10) & "Press E to exit")
        'Do While True
        '    Dim cki As ConsoleKeyInfo = Console.ReadKey(True)
        '    If (cki.Key = ConsoleKey.E) Then
        '        Return
        '    End If
        'Loop

    End Sub
    Private Sub LoadInfoBox_Today()
        Try

            'dt2.Clear()
            'Dim strCon As String = fnConnectionString()
            'Dim strSQL As String = "EXEC usp_IntervalDataLoader_DailySummary"

            'Dim da As New SqlDataAdapter(strSQL, strCon)
            'Try
            '    da.Fill(dt2)
            'Catch ex As OleDbException
            '    'MessageBox.Show(ex.Message)
            'End Try

            ''Me.DataGridView2.DataSource = Nothing
            ''Me.DataGridView2.DataSource = dt2

            'Dim lvwItem As ListViewItem
            'ListView1.Items.Clear()
            'For i As Integer = 0 To dt2.Rows.Count - 1

            '    lvwItem = New ListViewItem
            '    lvwItem.Text = dt2.Rows(i).Item("varApplicationName").ToString
            '    lvwItem.SubItems.Add(dt2.Rows(i).Item("intRecordsImported").ToString)

            '    ListView1.Items.Add(lvwItem)

            'Next i

            ''Console.WriteLine(Now & " | Information Box Refreshed")
        Catch ex As Exception
            Console.WriteLine("Load Info Box | " & ex.Message.ToString)
        End Try
    End Sub
    Private Sub ProcessTokWh()

        Core.data_execute_nonquery("EXEC UML_ExtData.dbo.usp_HHData_ProcessTokWh", fnConnectionString)

    End Sub
    Private Sub txtTimerInterval_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.txtTimerInterval.Text) = False Then Me.txtTimerInterval.Text = 1

    End Sub
    Private Sub btnAllFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllFiles.Click
        If Me.btnAllFiles.Text = "tick all boxes" Then
            Me.btnAllFiles.Text = "untick all boxes"
            Me.chkTrend.Checked = True
            Me.chkPhillips.Checked = True
            Me.chkIMServ.Checked = True
            Me.chkGeneric.Checked = True
            Me.chkRTUZFiles.Checked = True
            Me.chkFTPGazpromElecRecords.Checked = True
            Me.chkFTPTruReadElecRecords.Checked = True
            Me.chkCarillionaMT.Checked = True
            Me.chkIMServEmails.Checked = True
            Me.chkRTUZ.Checked = True
            Me.chkFTPGazpromElecFiles.Checked = True
            Me.chkFTPTruReadElecFiles.Checked = True
            Me.chkDataServices.Checked = True
            Me.chkTerminations.Checked = True
            Me.chkFilesStark.Checked = True
            Me.chkEmailStark.Checked = True
            Me.chkTenantStatments.Checked = True
            Me.chkUKFixedTerminationRequest.Checked = True
            Me.chkAutomatedReports.Checked = True
            Me.chkSEMOXMLFiles.Checked = True
            Me.chkBudgetRequests_MultiCountry.Checked = True
            Me.chkBudgetRequests_MultiClient.Checked = True
            Me.chkFTPCarillion.Checked = True
            Me.chkFilesCarillionFTP.Checked = True

        Else
            Me.btnAllFiles.Text = "tick all boxes"
            Me.chkTrend.Checked = False
            Me.chkPhillips.Checked = False
            Me.chkIMServ.Checked = False
            Me.chkGeneric.Checked = False
            Me.chkRTUZFiles.Checked = False
            Me.chkFTPGazpromElecRecords.Checked = False
            Me.chkFTPTruReadElecRecords.Checked = False
            Me.chkCarillionaMT.Checked = False
            Me.chkIMServEmails.Checked = False
            Me.chkRTUZ.Checked = False
            Me.chkFTPGazpromElecFiles.Checked = False
            Me.chkFTPTruReadElecFiles.Checked = False
            Me.chkDataServices.Checked = False
            Me.chkTerminations.Checked = False
            Me.chkFilesStark.Checked = False
            Me.chkEmailStark.Checked = False
            Me.chkTenantStatments.Checked = False
            Me.chkUKFixedTerminationRequest.Checked = False
            Me.chkAutomatedReports.Checked = False
            Me.chkSEMOXMLFiles.Checked = False
            Me.chkBudgetRequests_MultiCountry.Checked = False
            Me.chkBudgetRequests_MultiClient.Checked = False
            Me.chkFTPCarillion.Checked = False
            Me.chkFilesCarillionFTP.Checked = False

        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ProcessIMServFiles()
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblSummary.Text = Now & " | " & System.Environment.UserName
    End Sub
    Private Sub MissingSubMeterReport()

        Dim dt As New DataTable
        Dim dr As DataRow

        Dim CurrHour As Integer = DateTime.Now.Hour ' Get current hour value

        If CurrHour >= 7 Then ' If it is passed 7am
            Dim hasreportbeensent As Integer = -1
            hasreportbeensent = data_select_value("SELECT count(*) FROM UML_CMS.dbo.tblLog_MissingSubMeterReports WHERE dbo.formatdatetime(dtmcreated, 'dd/MM/yyyy') = dbo.formatdatetime(getdate(), 'dd/MM/yyyy')", fnConnectionString)

            If hasreportbeensent = 0 Then ' If the report hasnt already been sent
                dt = data_select("EXEC UML_CMS.dbo.usp_rpt_MissingSubMeterReport_Content", fnConnectionString)
                If dt.Rows.Count > 0 Then ' If there is something to send

                    Dim msgcontent As String = ""
                    Dim msgrecipients As String = ""
                    Dim customer As String = ""

                    msgcontent += "<b><u>The following sub meters were missing data at " & DateTime.Now & " for the previous day</u></b><br />"
                    For Each dr In dt.Rows
                        If Not customer = dr(0) Then
                            customer = dr(0)
                            msgcontent += "<br />Customer: <b>" & customer & "</b><br /><br />"
                        End If
                        msgcontent += dr(3) & " - " & dr(4) & " reads     (" & dr(1) & ", " & dr(2) & ")<br />"
                    Next

                    Dim dt_recipients As New DataTable
                    Dim dr_recipients As DataRow
                    dt_recipients = data_select("SELECT varEMailAddress FROM UML_CMS.dbo.tblLog_MissingSubMeterReport_Recipients WHERE bitDisabled = 0", fnConnectionString)
                    For Each dr_recipients In dt_recipients.Rows
                        SendSMTP("donotreply@ems.schneider-electric.com", dr_recipients(0), "Sub Meter Missing Data Report", msgcontent, "", "HTML")
                        msgrecipients &= ", " & dr_recipients(0)
                    Next

                    msgrecipients = msgrecipients.Remove(0, 2)
                    Dim insert_sql As String = "INSERT INTO UML_CMS.dbo.tblLog_MissingSubMeterReports (varReportContent, varReportRecipients) VALUES ('" & msgcontent & "', '" & msgrecipients & "')"
                    data_execute_nonquery(insert_sql, fnConnectionString)
                Else
                    Dim msgcontent As String = ""
                    Dim msgrecipients As String = ""
                    Dim customer As String = ""

                    msgcontent += "<b><u>There are no sub meters missing data at " & DateTime.Now & " for the previous day</u></b><br />"

                    Dim dt_recipients As New DataTable
                    Dim dr_recipients As DataRow
                    dt_recipients = data_select("SELECT varEMailAddress FROM UML_CMS.dbo.tblLog_MissingSubMeterReport_Recipients WHERE bitDisabled = 0", fnConnectionString)
                    For Each dr_recipients In dt_recipients.Rows
                        SendSMTP("donotreply@ems.schneider-electric.com", dr_recipients(0), "Sub Meter Missing Data Report", msgcontent, "", "HTML")
                        msgrecipients &= ", " & dr_recipients(0)
                    Next

                    msgrecipients = msgrecipients.Remove(0, 2)
                    Dim insert_sql As String = "INSERT INTO UML_CMS.dbo.tblLog_MissingSubMeterReports (varReportContent, varReportRecipients) VALUES ('" & msgcontent & "', '" & msgrecipients & "')"
                    data_execute_nonquery(insert_sql, fnConnectionString)
                End If
            End If
        End If

    End Sub

    Public Function CheckFileInUse(ByVal sFile As String) As Boolean
        If System.IO.File.Exists(sFile) Then
            Try
                Dim F As Short = CShort(FreeFile())
                FileOpen(F, sFile, OpenMode.Binary, OpenAccess.ReadWrite, OpenShare.LockReadWrite)
                FileClose(F)
            Catch
                Return True
            End Try
        End If
    End Function

    Protected Sub SaveURLtoFile(ByVal url As String, ByVal localpath As String)

        Dim loRequest As System.Net.HttpWebRequest
        Dim loResponse As System.Net.HttpWebResponse
        Dim loResponseStream As System.IO.Stream
        Dim loFileStream As New System.IO.FileStream(localpath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim laBytes(256) As Byte
        Dim liCount As Integer = 1

        Try
            loRequest = CType(System.Net.WebRequest.Create(url), System.Net.HttpWebRequest)
            loRequest.Credentials = New System.Net.NetworkCredential("interval_data", "Ultr@b00k", "mceg.local")
            loRequest.Timeout = 1000 * 60 * 15 'timeout 15 minutes
            loRequest.Method = "GET"
            loResponse = CType(loRequest.GetResponse, System.Net.HttpWebResponse)
            loResponseStream = loResponse.GetResponseStream
            Do While liCount > 0
                liCount = loResponseStream.Read(laBytes, 0, 256)
                loFileStream.Write(laBytes, 0, liCount)
            Loop
            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()

        Catch ex As Exception

            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()
            'System.IO.File.Delete(localpath)
        End Try

    End Sub
    Protected Sub MCEGSaveURLtoFile(ByVal url As String, ByVal localpath As String)

        Dim loFileStream As New System.IO.FileStream(localpath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim loRequest As System.Net.HttpWebRequest
        Dim loResponse As System.Net.HttpWebResponse
        Dim loResponseStream As System.IO.Stream
        Dim laBytes(256) As Byte
        Dim liCount As Integer = 1
        Try

            loRequest = CType(System.Net.WebRequest.Create(url), System.Net.HttpWebRequest)
            loRequest.Credentials = New System.Net.NetworkCredential("dave.clarke", "Purp13m0nkey""", "MCEG.local")
            loRequest.Timeout = 1000 * 60 * 15 'timeout 15 minutes
            loRequest.Method = "GET"
            loResponse = CType(loRequest.GetResponse, System.Net.HttpWebResponse)
            loResponseStream = loResponse.GetResponseStream

            
            Do While liCount > 0
                liCount = loResponseStream.Read(laBytes, 0, 256)
                loFileStream.Write(laBytes, 0, liCount)
            Loop
            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()

        Catch ex As Exception

            loFileStream.Flush()
            loFileStream.Close()
            loFileStream.Dispose()
        End Try

    End Sub

    Private Sub ProcessDataServices()

        Dim dtdataservices As New DataTable
        Dim dr As DataRow

        data_execute_nonquery("EXEC UML_CMS.dbo.usp_DataServices_GenerateSchedule", fnConnectionString)

        dtdataservices = data_select("EXEC UML_CMS.dbo.usp_AUTOMATION_DataServices", fnConnectionString)
        If dtdataservices.Rows.Count > 0 Then ' If there is something to send
            ' Console.WriteLine(Now & " | Data Services - Start (" & CStr(dtdataservices.Rows.Count) & ")")

            For Each dr In dtdataservices.Rows

                Dim LocationString As String
                Dim strURL As String
                Dim strPath As String

                LocationString = dr("varUNCPATH")
                strURL = dr("varReportServerURL") + dr("varReport") + "&rs:Format=" + dr("Format")
                strPath = LocationString + dr("varFileName") + "." + dr("Format")

                If Directory.Exists(LocationString) Then
                    If File.Exists(strPath) Then
                        If CheckFileInUse(strPath) = False Then
                            'File.Delete(strPath)
                            'SaveURLtoFile(strURL, strPath)
                        End If
                    Else
                        SaveURLtoFile(strURL, strPath)
                    End If
                Else
                    Directory.CreateDirectory(LocationString)
                    SaveURLtoFile(strURL, strPath)
                End If

                'SaveURLtoFile(strURL, strPath)

                Dim UpdateScheduleQuery As String = "UPDATE uml_cms.dbo.[tblDServicesLog] SET [DSsentdate] = getdate(), [DSsent] = -1, [DSsentby] = 'System' WHERE [PKDSlogID] = " + CStr(dr("PKDSlogID"))

                Dim htmlMessageTo As String = dr("varRecipient")
                Dim htmlMessageFrom As String = "donotreply@ems.schneider-electric.com"
                Dim htmlMessageSubject As String = dr("varSubject")
                Dim htmlMessageAttachment As String = dr("varAttachment")

                Dim htmlMessageBody As String

                htmlMessageBody = " "
                htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
                htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
                htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
                htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
                htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
                htmlMessageBody = htmlMessageBody + " </STYLE>"
                htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
                htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
                htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>"
                htmlMessageBody = htmlMessageBody + " <TABLE>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
                htmlMessageBody = htmlMessageBody + " <P><B>"
                htmlMessageBody = htmlMessageBody + dr("varBody")
                htmlMessageBody = htmlMessageBody + ", </B> </P>"
                htmlMessageBody = htmlMessageBody + " <P>Please find attached your "
                htmlMessageBody = htmlMessageBody + dr("varSubject")
                htmlMessageBody = htmlMessageBody + " </P>"
                htmlMessageBody = htmlMessageBody + " <P>This report details your electricity use, "
                htmlMessageBody = htmlMessageBody + " showing your consumption each day and by your "
                htmlMessageBody = htmlMessageBody + " chargeable time bands.  A detailed daily breakdown "
                htmlMessageBody = htmlMessageBody + " of your consumption per half hour against the average "
                htmlMessageBody = htmlMessageBody + " in the month is given, highlighting your actual time "
                htmlMessageBody = htmlMessageBody + " of Maximum Demand in the month.</P>"
                htmlMessageBody = htmlMessageBody + " <P>If you have any questions about this report, or if "
                htmlMessageBody = htmlMessageBody + " you would like to discuss how Schneider Electric can help you to improve your energy use, please contact your account manager "
                htmlMessageBody = htmlMessageBody + dr("accountmgr")
                htmlMessageBody = htmlMessageBody + ".</P>"
                htmlMessageBody = htmlMessageBody + " </TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
                htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
                htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
                htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
                htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"

                SendSMTP("donotreply@ems.schneider-electric.com", htmlMessageTo, htmlMessageSubject, htmlMessageBody, htmlMessageAttachment, "HTML", "", "", "")

                data_execute_nonquery(UpdateScheduleQuery, fnConnectionString)

                Console.WriteLine("Processed - " + dr("varFileName") + " | " & Now)

            Next
            'Console.WriteLine(Now & " | Data Services - End")
        Else
            ' Console.WriteLine(Now & " | Data Services - None to process")
        End If

    End Sub

    Private Sub ProcessTerminations()


        Dim dtterminations As New DataTable
        Dim dr As DataRow

        dtterminations = data_select("EXEC [UML_CMS].[UML_CMS].[Auto_CompleteTerminationLetters]", fnConnectionString)
        If dtterminations.Rows.Count > 0 Then ' If there is something to send
            Dim startnumber As Integer = data_select_value("SELECT COUNT(*) FROM UML_CMS.dbo.tbltermination_schedule", fnConnectionString)
            'Console.WriteLine(Now & " | Terminations - Start (" & CStr(startnumber.ToString) & ")")

            For Each dr In dtterminations.Rows
                Dim UpdateScheduleQuery As String = dr("varTable")
                Dim DeleteScheduleQuery As String = dr("deletetable")

                Dim LocationString As String = dr("varUNCPath")
                Dim LocationString2 As String = dr("varUNCPath2")
                Dim strURL As String = dr("varReportServerURL") + dr("varReport") + "&rs:Format=" + dr("Format")
                Dim strPath As String = LocationString + dr("varFileName") + "." + dr("Format")
                Dim strPath2 As String = LocationString2 + dr("varFileName") + "." + dr("Format")

                Dim htmlMessageTo As String = dr("varRecipient")
                Dim htmlMessageFrom As String = "terminations@ems.schneider-electric.com"
                Dim htmlMessageSubject As String = dr("varSubject")
                Dim htmlMessageAttachment As String = dr("varAttachment")
                Dim htmlMessageBcc As String = "terminations_sent@ems.schneider-electric.com"

                Dim htmlMessageBody As String = ""

                If Directory.Exists(LocationString) Then
                    If File.Exists(strPath) Then

                    Else
                        'MsgBox("File Created !!")
                        SaveURLtoFile(strURL, strPath)
                    End If
                Else
                    Directory.CreateDirectory(LocationString)
                    SaveURLtoFile(strURL, strPath)
                End If

                'If Directory.Exists(LocationString2) Then
                'If File.Exists(strPath2) Then
                'Else
                '    'MsgBox("File Created !!")
                '    SaveURLtoFile(strURL, strPath2)
                'End If
                'Else
                '    Directory.CreateDirectory(LocationString2)
                '    SaveURLtoFile(strURL, strPath2)
                'End If

                    htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
                    htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
                    htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
                    htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma}"
                    htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #082b61; FONT-FAMILY: Tahoma;}"
                    htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: green; FONT-FAMILY: Tahoma;}"
                    htmlMessageBody = htmlMessageBody + " </STYLE>"
                    htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
                    htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #082b61; FONT-FAMILY: Tahoma'>"
                    htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                    htmlMessageBody = htmlMessageBody + " <TBODY>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>"
                    htmlMessageBody = htmlMessageBody + " <TABLE>"
                    htmlMessageBody = htmlMessageBody + " <TBODY>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
                    htmlMessageBody = htmlMessageBody + " <P><B>"
                    htmlMessageBody = htmlMessageBody + dr("varBody")
                    htmlMessageBody = htmlMessageBody + ", </B> </P>"

                    htmlMessageBody = htmlMessageBody + " <P>Please confirm receipt and acceptance by return email.</P>"

                    htmlMessageBody = htmlMessageBody + " </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD>Regards, </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD>Terminations Department </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD>"
                    htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                    htmlMessageBody = htmlMessageBody + " <TBODY>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
                    htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                    htmlMessageBody = htmlMessageBody + " <TBODY>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD><B>M&C Energy Group</B> </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>5 Salmon Fields Business Village </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
                    htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                    htmlMessageBody = htmlMessageBody + " <TBODY>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
                    htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.mcenergygroup.com' "
                    htmlMessageBody = htmlMessageBody + " target=_blank>www.mcenergygroup.com</A> "
                    htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD></TD>"
                    htmlMessageBody = htmlMessageBody + " <TD class=smallblue colSpan=2>Help cut carbon. Please dont print "
                    htmlMessageBody = htmlMessageBody + " this email unless you really need to. </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR></TR></TBODY></TABLE></TD></TR>"
                    htmlMessageBody = htmlMessageBody + " <TR>"
                    htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"


                    SendSMTP(htmlMessageFrom, htmlMessageTo, htmlMessageSubject, htmlMessageBody, htmlMessageAttachment, "HTML", htmlMessageBcc, "", "")

                    data_execute_nonquery(UpdateScheduleQuery, fnConnectionString)
                    data_execute_nonquery(DeleteScheduleQuery, fnConnectionString)

                    Dim remaining As Integer = data_select_value("SELECT COUNT(*) FROM UML_CMS.dbo.tbltermination_schedule", fnConnectionString)

                    Console.WriteLine("(" + remaining.ToString + ") Processed - " + dr("varFileName") + " | " & Now)

            Next


            'Console.WriteLine(Now & " | Terminations - End")
            'Else
            '    Console.WriteLine(Now & " | Terminations - None to process")
        End If

    End Sub

    Private Sub ProcessTenantStatements()
        Dim dtTenantStatement As New DataTable
        Dim dr As DataRow

        dtTenantStatement = data_select("EXEC [UML_CMS].[dbo].[usp_AUTOMATION_TenantStatement]", fnConnectionString)
        If dtTenantStatement.Rows.Count > 0 Then ' If there is something to send
            'Console.WriteLine(Now & " | Tenant Statement - Start (" & CStr(dtTenantStatement.Rows.Count) & ")")

            For Each dr In dtTenantStatement.Rows
                Dim UpdateScheduleQuery As String = dr("varTable")

                Dim LocationString As String = dr("varUNCPath")
                Dim strURL As String = dr("varReportServerURL") + dr("varReport") + "&rs:Format=" + dr("Format")
                Dim strPath As String = LocationString + dr("varFileName") + "." + dr("Format")

                If Directory.Exists(LocationString) Then
                    If File.Exists(strPath) Then
                        If CheckFileInUse(strPath) = False Then
                            'File.Delete(strPath)
                            'SaveURLtoFile(strURL, strPath)
                        End If
                    Else
                        SaveURLtoFile(strURL, strPath)
                    End If
                Else
                    Directory.CreateDirectory(LocationString)
                    SaveURLtoFile(strURL, strPath)
                End If



                Dim htmlMessageTo As String = dr("varRecipient")
                Dim htmlMessageFrom As String = "donotreply@ems.schneider-electric.com"
                Dim htmlMessageSubject As String = dr("varSubject")
                Dim htmlMessageAttachment As String = strPath
                Dim htmlMessageBcc As String = ""

                Dim htmlMessageBody As String = ""

                htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Schneider Electric</TITLE>"
                htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
                htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
                htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
                htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
                htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: green; FONT-FAMILY: Tahoma;}"
                htmlMessageBody = htmlMessageBody + " </STYLE>"
                htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
                htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
                htmlMessageBody = htmlMessageBody + " > </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>"
                htmlMessageBody = htmlMessageBody + " <TABLE>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
                htmlMessageBody = htmlMessageBody + " <P><B>"
                htmlMessageBody = htmlMessageBody + dr("varBody")
                htmlMessageBody = htmlMessageBody + ", </B> </P>"
                htmlMessageBody = htmlMessageBody + " <P>If you have any questions about this report, or if "
                htmlMessageBody = htmlMessageBody + " you would like to discuss how Schneider Electric can help you to improve your energy use, please contact your account manager "
                htmlMessageBody = htmlMessageBody + dr("accountmgr")
                htmlMessageBody = htmlMessageBody + ".</P>"
                htmlMessageBody = htmlMessageBody + " </TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD>Regards, </TD>"
                htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD><B>Paul Stevens</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD>Information Services Administrator </TD>"
                htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD>"
                htmlMessageBody = htmlMessageBody + " <TD>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>5 Salmon Fields Business Village </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
                htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
                htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
                htmlMessageBody = htmlMessageBody + " <TBODY>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
                htmlMessageBody = htmlMessageBody + " <TR>"
                htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
                htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
                htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
                htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
                htmlMessageBody = htmlMessageBody + " </TBODY></TABLE></BODY>"

                SendSMTP(htmlMessageFrom, htmlMessageTo, htmlMessageSubject, htmlMessageBody, htmlMessageAttachment, "HTML", "", "", "")

                data_execute_nonquery(UpdateScheduleQuery, fnConnectionString)

                Console.WriteLine("Processed - " + dr("varFileName") + " | " & Now)

            Next
            'Console.WriteLine(Now & " | Tenant Statement - End")
        Else
            'Console.WriteLine(Now & " | Tenant Statement - None to process")
        End If




    End Sub

    Dim mdt As DataTable
    Dim mdr As DataRow
    Dim dr As DataRow
    Dim cm As SqlClient.SqlCommand
    Dim CDC As DataCommon
    Dim CO As New SqlCommand()

    Private Sub ProcessAutomatedReports()
        Dim dtar As New DataTable
        dtar = data_select("EXEC se_cca.dbo.rsp_reportautomations_schedule", fnConnectionString)

        'Console.WriteLine(Now & " | Automated Reports - Start (" & CStr(dtar.Rows.Count) & ")")

        For Each dr In dtar.Rows

            Dim strURL As String = dr("reportserverurl") + dr("reportpath") '+ "&rs:Format=" + dr("formatname")
            'Dim strFilepath As String = dr("filepath") & dr("filename")

            'If Directory.Exists(dr("filepath")) Then
            '    If Not File.Exists(strFilepath) Then
            '        MCEGSaveURLtoFile(strURL, strFilepath)
            '    End If
            'Else
            '    Directory.CreateDirectory(dr("filepath"))
            '    MCEGSaveURLtoFile(strURL, strFilepath)
            'End If


            Dim htmlMessageTo As String = dr("email")
            Dim htmlMessageFrom As String = "donotreply@ems.schneider-electric.com"
            Dim htmlMessageSubject As String = dr("reportname")
            Dim htmlMessageAttachment As String = "" 'dr("filepath") & dr("filename")

            Dim htmlMessageBody As String

            htmlMessageBody = " "
            htmlMessageBody = htmlMessageBody + " <HTML xmlns='http://www.w3.org/1999/xhtml'><HEAD><TITLE>Utility Masters Ltd</TITLE>"
            htmlMessageBody = htmlMessageBody + " <META http-equiv=Content-Type content='text/html; charset=utf-8'>"
            htmlMessageBody = htmlMessageBody + " <STYLE type=text/css>"
            htmlMessageBody = htmlMessageBody + " a {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " body {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " a:link {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " a:visited {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " a:active {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " a:hover {FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " td {FONT-SIZE: 10pt; COLOR: #000000; FONT-FAMILY: Tahoma}"
            htmlMessageBody = htmlMessageBody + " td.smallblue {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
            htmlMessageBody = htmlMessageBody + " td.green {FONT-SIZE: 7pt; COLOR: #009530; FONT-FAMILY: Tahoma;}"
            htmlMessageBody = htmlMessageBody + " </STYLE>"
            htmlMessageBody = htmlMessageBody + " <META content='MSHTML 6.00.6000.16674' name=GENERATOR></HEAD>"
            htmlMessageBody = htmlMessageBody + " <BODY style='FONT-SIZE: 10pt; COLOR: #009530; FONT-FAMILY: Tahoma'>"
            htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
            htmlMessageBody = htmlMessageBody + " <TBODY>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD><IMG height=78 alt='Schneider Electric Logo' src='http://images.utilitymasters.co.uk/system_emails/_schneider/branding-header.png' "
            htmlMessageBody = htmlMessageBody + " width=750> </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>"
            htmlMessageBody = htmlMessageBody + " <TABLE>"
            htmlMessageBody = htmlMessageBody + " <TBODY>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD>"
            htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 550px'>"
            htmlMessageBody = htmlMessageBody + " <P><B>Dear "
            htmlMessageBody = htmlMessageBody + dr("name")
            htmlMessageBody = htmlMessageBody + ", </B> </P>"
            htmlMessageBody = htmlMessageBody + " <P>"
            htmlMessageBody = htmlMessageBody + dr("body")
            htmlMessageBody = htmlMessageBody + " </P>"
            htmlMessageBody = htmlMessageBody + " <P>"
            htmlMessageBody = htmlMessageBody + " <a href='" + strURL + "' target='_blank'>View Report</a> "
            htmlMessageBody = htmlMessageBody + " </P>"
            htmlMessageBody = htmlMessageBody + " </TD>"
            htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'></TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD></TD>"
            htmlMessageBody = htmlMessageBody + " <TD>Regards</TD>"
            htmlMessageBody = htmlMessageBody + " <TD></TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD></TD>"
            htmlMessageBody = htmlMessageBody + " <TD>"
            htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
            htmlMessageBody = htmlMessageBody + " <TBODY>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 280px'>"
            htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
            htmlMessageBody = htmlMessageBody + " <TBODY>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD><B>Schneider Electric</B> </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>Unit 4-5 </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>Salmon Fields Business Village </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>Royton </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>Oldham </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>OL2 6HT </TD></TR></TBODY></TABLE></TD>"
            htmlMessageBody = htmlMessageBody + " <TD style='FONT-SIZE: 12pt; WIDTH: 280px'>"
            htmlMessageBody = htmlMessageBody + " <TABLE cellSpacing=0 cellPadding=0>"
            htmlMessageBody = htmlMessageBody + " <TBODY>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD style='WIDTH: 20px'><B>T</B> </TD>"
            htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 0404 </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD><B>F</B> </TD>"
            htmlMessageBody = htmlMessageBody + " <TD>+44 (0)161 785 7969 </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD><B>W</B> </TD>"
            htmlMessageBody = htmlMessageBody + " <TD><A href='http://www.schneider-electric.com' "
            htmlMessageBody = htmlMessageBody + " target=_blank>www.schneider-electric.com</A> "
            htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD>&nbsp; </TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right' colSpan=3> "
            htmlMessageBody = htmlMessageBody + " </TD></TR></TBODY></TABLE></TD></TR>"
            htmlMessageBody = htmlMessageBody + " <TR>"
            htmlMessageBody = htmlMessageBody + " <TD style='TEXT-ALIGN: right'></TD></TR></TBODY></TABLE></BODY>"

            SendSMTP("donotreply@ems.schneider-electric.com", htmlMessageTo, htmlMessageSubject, htmlMessageBody, htmlMessageAttachment, "HTML", "", "", "")

            Dim strUpdate As String = "INSERT INTO [se_cca].[dbo].[logreportautomations]"
            strUpdate &= "([creator_fk],[editor_fk],[created],[modified],[rowstatus],[reportautomation_fk])"
            strUpdate &= "VALUES (35, 35, GETDATE(), GETDATE(), 0, " & dr("reportautomation_pk") & ")"

            data_execute_nonquery(strUpdate, fnConnectionString)


            Console.WriteLine("Processed - " + dr("filename") + " | " & Now)

        Next

        'Console.WriteLine(Now & " | Automated Reports - End")

    End Sub

    Private Sub ProcessSurveys()





    End Sub

    Private Sub CheckSEMOPricing()
        ' This is to see if todays file has been processed yet

        Dim sql_check As String = ""

        sql_check = "SELECT COUNT(*) FROM [UML_ExtData].[dbo].[tblsemopricingfiles] WHERE downloaddate = '" & Now.Date & "'"

        If Core.data_select_value(sql_check, fnConnectionString) = 0 Then
            ProcessSEMOPricingWebsite()
        End If

        sql_check = "SELECT COUNT(*) FROM [UML_ExtData].[dbo].[tblsemopricingfiles] WHERE [processed] = 0"

        If Core.data_select_value(sql_check, fnConnectionString) > 0 Then
            ProcessSEMOXMLFiles()
        End If


    End Sub

    Private Sub ProcessSEMOPricingWebsite()
        Try

            Dim base_url As String = "http://www.sem-o.com"

            Dim request_1 As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(base_url & "/MarketData/pages/pricingandscheduling.aspx")
            Dim response_1 As System.Net.HttpWebResponse = request_1.GetResponse()
            Dim sr_1 As System.IO.StreamReader = New System.IO.StreamReader(response_1.GetResponseStream())
            Dim sourcecode As String = sr_1.ReadToEnd()

            Dim reportsfolder As String = "/MarketReports/StaticReports/"
            Dim startoffilewewant As String = "EX-ANTE MARKET SCHEDULE SUMMARY_WD1_"
            
            Dim FirstCharacter As Integer = sourcecode.IndexOf(startoffilewewant)
            Dim filewewant As String = ""
            If FirstCharacter > -1 Then
                filewewant = sourcecode.Substring(FirstCharacter, 64)
                Dim stringdate = filewewant.Replace(startoffilewewant, "").Substring(0, 8)
                Dim downloaddate_year As String = stringdate.Substring(0, 4)
                Dim downloaddate_month As String = stringdate.Substring(4, 2)
                Dim downloaddate_day As String = stringdate.Substring(6, 2)
                Dim download_date As Date = CDate(downloaddate_day & "/" & downloaddate_month & "/" & downloaddate_year)
                Dim str_sql As String = "INSERT INTO [uml_extdata].[dbo].[tblsemopricingfiles] ([url],[downloaddate]) values ('" & base_url & reportsfolder & filewewant & "', '" & download_date & "')"
                Core.data_execute_nonquery(str_sql, fnConnectionString)

                Console.WriteLine(Now & " | SEMO - d/l (" & download_date & ")")
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ProcessSEMOXMLFiles()

        Try

            Dim dt As New DataTable
            dt = Core.data_select("SELECT * FROM [uml_extdata].[dbo].[tblsemopricingfiles] WHERE [processed] = 0", fnConnectionString) '")

            For Each dr As DataRow In dt.Rows

                Dim request_2 As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(dr("url"))
                Dim response_2 As System.Net.HttpWebResponse = request_2.GetResponse()
                Dim sr_2 As System.IO.StreamReader = New System.IO.StreamReader(response_2.GetResponseStream())
                Dim xmlcode As String = sr_2.ReadToEnd()

                Dim xmlFile As XmlReader
                Dim ds As New DataSet
                Dim i As Integer

                Dim var_semopricingfile_fk As Integer = -1
                Dim var_tradedate As Date
                Dim var_deliverydate As Date
                Dim var_deliveryhour As Integer = -1
                Dim var_deliveryinterval As Integer = -1
                Dim var_runtype As String = ""
                Dim var_aggregatedmsq As Decimal = 0
                Dim var_smp As Decimal = 0
                Dim var_currencyflag As String = ""

                Dim sql_check As String = ""
                Dim sql_insert As String = ""

                xmlFile = XmlReader.Create(dr("url"), New XmlReaderSettings())
                ds.ReadXml(xmlFile)
                For i = 0 To ds.Tables(4).Rows.Count - 1

                    var_semopricingfile_fk = dr("semopricingfile_pk")
                    var_tradedate = ds.Tables(4).Rows(i).Item(0)
                    var_deliverydate = ds.Tables(4).Rows(i).Item(1)
                    var_deliveryhour = ds.Tables(4).Rows(i).Item(2)
                    var_deliveryinterval = ds.Tables(4).Rows(i).Item(3)
                    var_runtype = ds.Tables(4).Rows(i).Item(4)
                    var_aggregatedmsq = ds.Tables(4).Rows(i).Item(5)
                    var_smp = ds.Tables(4).Rows(i).Item(6)
                    var_currencyflag = ds.Tables(4).Rows(i).Item(7)

                    sql_check = "SELECT COUNT(*) FROM [UML_ExtData].[dbo].[tblsemopricings]"
                    sql_check &= " WHERE tradedate = '" & var_tradedate & "'"
                    sql_check &= " AND deliverydate = '" & var_deliverydate & "'"
                    sql_check &= " AND deliveryhour = " & var_deliveryhour & ""
                    sql_check &= " AND deliveryinterval = " & var_deliveryinterval & ""
                    sql_check &= " AND runtype = '" & var_runtype & "'"
                    sql_check &= " AND aggregatedmsq = '" & var_aggregatedmsq & "'"
                    sql_check &= " AND smp = '" & var_smp & "'"
                    sql_check &= " AND currencyflag = '" & var_currencyflag & "'"

                    sql_insert = "Insert INTO [UML_ExtData].[dbo].[tblsemopricings] ([creator_fk], [editor_fk], [semopricingfile_fk], [tradedate], [deliverydate], [deliveryhour], [deliveryinterval], [runtype], [aggregatedmsq], [smp], [currencyflag])"
                    sql_insert &= "VALUES (31, 31, '" & var_semopricingfile_fk & "', '" & var_tradedate & "', '" & var_deliverydate & "', " & var_deliveryhour & ", " & var_deliveryinterval & ", '" & var_runtype & "', '" & var_aggregatedmsq & "', '" & var_smp & "', '" & var_currencyflag & "')"

                    If Core.data_select_value(sql_check, fnConnectionString) = 0 Then
                        Core.data_select_value(sql_insert, fnConnectionString)
                    End If

                Next
                
                Dim str_date As String = CStr(dr("downloaddate"))
                Dim str_filenamedate As String = DatePart(DateInterval.Year, dr("downloaddate")) & "."
                If DatePart(DateInterval.Month, dr("downloaddate")) < 10 Then str_filenamedate &= "0" & DatePart(DateInterval.Month, dr("downloaddate")) Else str_filenamedate &= DatePart(DateInterval.Month, dr("downloaddate"))
                str_filenamedate &= "."
                If DatePart(DateInterval.Day, dr("downloaddate")) < 10 Then str_filenamedate &= "0" & DatePart(DateInterval.Day, dr("downloaddate")) Else str_filenamedate &= DatePart(DateInterval.Day, dr("downloaddate"))


                Console.WriteLine(Now & " | SEMO - done (" & str_date & ")")

                'Dim strURL As String = "http://uml-sql/reportserver?%2fSolutions+Team/SEMO+Pricing+Report&tradedate=" & str_date & "&rs:Format=EXCEL"
                'Dim strFolderPath As String = "\\UK-OL1-FILE-01\Group Shares\Data\SEMO Pricing\"
                'Dim strFilepath As String = str_filenamedate & " - " & var_runtype & " SEMO Pricing.xls"

                'If Directory.Exists(strFolderPath) Then
                '    If Not File.Exists(strFolderPath & strFilepath) Then
                '        MCEGSaveURLtoFile(strURL, strFolderPath & strFilepath)
                '    End If
                'Else
                '    Directory.CreateDirectory(strFolderPath)
                '    MCEGSaveURLtoFile(strURL, strFolderPath & strFilepath)
                'End If

                'Kieran.O'Driscoll@ems.schneider-electric.com
                'dave.clarke@ems.schneider-electric.com

                SendSMTP("donotreply@ems.schneider-electric.com", "Kieran.O'Driscoll@ems.schneider-electric.com", str_date & " | SEMO File Processed", "", "", "HTML", "", "", "")

                Core.data_execute_nonquery("UPDATE [uml_extdata].[dbo].[tblsemopricingfiles] SET [processed] = 1 WHERE [semopricingfile_pk] = " & dr("semopricingfile_pk"), fnConnectionString)

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Process_BudgetRequests_MultiCountry()
        Try

            ' Get a list of all "OPEN" multi-country requests

            Dim strOpen As String = "USE [servicedesk] "
            strOpen &= "SELECT "
            strOpen &= "a.WORKORDERID"
            strOpen &= ", a.REQUESTERID "
            strOpen &= ", a.CREATEDBYID "
            strOpen &= ", a.TITLE "
            strOpen &= ", a.DESCRIPTION "
            strOpen &= ", c.STATUSID "
            strOpen &= ", c.OWNERID "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= a.CREATEDBYID)		[creator] "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= a.REQUESTERID)		[requester] "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= c.OWNERID)			[technician] "
            strOpen &= ", d.GUDF_CHAR1 [customer]"
            strOpen &= ", CASE WHEN LEN(CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR)) = 1 THEN '0' + CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR) ELSE CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR) END + ' ' + LEFT(datename(month, [se].[fn_GetDateTime](UDF_DATE2)), 3) + ' ' + cast(datepart(year, [se].[fn_GetDateTime](UDF_DATE2)) as varchar) [duedate] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE1) [date1] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE2) [date2] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE3) [date3] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE4) [date4] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE5) [date5] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE6) [date6] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE7) [date7] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE8) [date8] "
            strOpen &= ", [se].[fn_GetDateTime](a.CREATEDTIME) [createddate] "
            strOpen &= ", b.* "
            strOpen &= "FROM "
            strOpen &= "[dbo].[WorkOrder] a "
            strOpen &= "INNER JOIN [dbo].[ServiceReq_5101] b on a.WORKORDERID = b.WORKORDERID "
            strOpen &= "INNER JOIN [dbo].[workorderstates] c on a.workorderid = c.workorderid "
            strOpen &= "INNER JOIN [dbo].[servicecatalog_fields] d ON a.WORKORDERID = d.WORKORDERID "
            strOpen &= "WHERE a.TEMPLATEID = 11701 " 'This template is the multi country id
            strOpen &= "AND a.COMPLETEDTIME = 0 " ' basically not already completed

            Dim dtOpen As New DataTable
            dtOpen = data_select(strOpen, fnConnectionStringHD)

            Dim strHD As String = fnHelpdeskURL() & fnHelpdeskRESTAPIURL()
            Dim operation As String = ""

            Dim strDescription As String = ""
            Dim reqtemplate As String = ""
            Dim strRequester As String = ""
            Dim strSite As String = ""
            Dim strGroup As String = ""
            Dim strCustomer As String = ""
            Dim strCommodity As String = ""
            Dim strDueDate As String = ""
            Dim strTitle As String = ""

            Dim outcome As String = ""


            Dim strXML As String = ""

            If dtOpen.Rows.Count > 0 Then

                Console.WriteLine(Now & " | Multi Country Budget Requests - Start (" & dtOpen.Rows.Count & ")")

                For Each drOpen As DataRow In dtOpen.Rows

                    Dim recordedids As String = ""

                    Dim strCountries As String = "USE [servicedesk] "
                    strCountries &= "SELECT b.ANSWER [answer] "
                    strCountries &= ", right(b.ANSWER, (len(b.ANSWER)-(CHARINDEX('|',b.ANSWER))-1)) [country] "
                    strCountries &= ", left(b.ANSWER, (CHARINDEX('|',b.ANSWER)-2)) [countrycode] "
                    strCountries &= "FROM WO_Resources a "
                    strCountries &= "LEFT JOIN ResourcesQAMapping b ON a.UID=b.MAPPINGID "
                    strCountries &= "LEFT JOIN CatalogResource c ON a.RESOURCEID=c.UID "
                    strCountries &= "LEFT JOIN Questions d ON d.QUESTIONID=b.QUESTIONID "
                    strCountries &= "WHERE WOID = " & drOpen("WORKORDERID")

                    Dim dtCountries As New DataTable
                    dtCountries = data_select(strCountries, fnConnectionStringHD)
                    Console.WriteLine(Now & " | RequestID: " & drOpen("WORKORDERID") & " | " & dtCountries.Rows.Count & " countries")
                    If dtCountries.Rows.Count > 0 Then
                        For Each drCountries As DataRow In dtCountries.Rows

                            strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL()
                            operation = "ADD_REQUEST"
                            strDescription = "This requests was automatically created from the Multi Country request ID: " & drOpen("WORKORDERID") & vbCrLf & vbCrLf & drOpen("DESCRIPTION")
                            reqtemplate = "Budget Requests"
                            strRequester = drOpen("requester")
                            strSite = "Budget Requests"

                            If drOpen("UDF_CHAR14") = "No" Then 'default group
                                strGroup = "* Default Group"
                            Else 'country specific
                                strGroup = drCountries("country")
                            End If

                            strCustomer = drOpen("customer")
                            strCommodity = drOpen("UDF_CHAR5")
                            strDueDate = drOpen("duedate")
                            strTitle = "Budget Request - " & strCustomer & " | " & drCountries("countrycode") & " | " & strCommodity & " | " & strDueDate


                            strXML = ""

                            strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                            strXML &= "<Operation>"
                            strXML &= "<Details>"

                            strXML &= "<parameter>"
                            strXML &= "<name>requesttemplate</name>"
                            strXML &= "<value>" & reqtemplate & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>requester</name>"
                            strXML &= "<value>" & strRequester & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>site</name>"
                            strXML &= "<value>" & strSite & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>group</name>"
                            strXML &= "<value>" & strGroup & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Subject</name>"
                            strXML &= "<value>" & Replace(strTitle, "&", "and") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Description</name>"
                            strXML &= "<value>" & Replace(Replace(strDescription, "&", "and"), "#", "") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Customer</name>"
                            strXML &= "<value>" & Replace(strCustomer, "&", "and") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>CBMS Budget ID</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR1") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Other Comments</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR2") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Query Contact</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR3") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Completion Contact</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR4") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Commodity</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR5") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Commodity Methodology</name>"
                            strXML &= "<value>" & Replace(drOpen("UDF_CHAR6"), "+", "and") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Annual Budgeting Service Only</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR14") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Consumption Methodology</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR7") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Reforecast?</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR8") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Reviewed  By</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR9") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Country Code</name>"
                            strXML &= "<value>" & drCountries("countrycode") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Requestor OLA Breach</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR11") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>CBMS Budget?</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR12") & "</value>"
                            strXML &= "</parameter>"

                            If Not IsDBNull(drOpen("date1")) Then

                                Dim date1 As Date = drOpen("date1")
                                Dim date1_string As String = date1.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Client Inputs Finalised Date</name>"
                                strXML &= "<value>" & date1_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date2")) Then

                                Dim date2 As Date = drOpen("date2")
                                Dim date2_string As String = date2.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Desired Due Date</name>"
                                strXML &= "<value>" & date2_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date3")) Then


                                Dim date3 As Date = drOpen("date3")
                                Dim date3_string As String = date3.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Budget From Date</name>"
                                strXML &= "<value>" & date3_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date4")) Then

                                Dim date4 As Date = drOpen("date4")
                                Dim date4_string As String = date4.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Missing Inputs Acquired Date</name>"
                                strXML &= "<value>" & date4_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date5")) Then

                                Dim date5 As Date = drOpen("date5")
                                Dim date5_string As String = date5.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Desired Price Date</name>"
                                strXML &= "<value>" & date5_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date6")) Then

                                Dim date6 As Date = drOpen("date6")
                                Dim date6_string As String = date6.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Assumptions Agreed Date</name>"
                                strXML &= "<value>" & date6_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date7")) Then

                                Dim date7 As Date = drOpen("date7")
                                Dim date7_string As String = date7.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Costs Modelled Date</name>"
                                strXML &= "<value>" & date7_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date8")) Then

                                Dim date8 As Date = drOpen("date8")
                                Dim date8_string As String = date8.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Internal Review Date</name>"
                                strXML &= "<value>" & date8_string & "</value>"
                                strXML &= "</parameter>"

                            End If



                            strXML &= "</Details>"
                            strXML &= "</Operation>"

                            outcome = PostTo(strHD, strXML)

                            If outcome.StartsWith("Success: ") Then
                                recordedids &= Replace(outcome, "Success", drCountries("country")) & vbCrLf
                                RecordLogRecord("Multi Country Budget Requests", outcome, "Success", 1)
                            Else
                                SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Country Budget Request Import Error | " & drOpen("WORKORDERID"), "Adding " & drCountries("country") & " request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")
                            End If

                        Next

                        ' this point all the NEW requests are done.. 
                        ' update the original to include all the child id's in the desc
                        ' close down the original multi country request 

                        strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL() & drOpen("WORKORDERID") & "/"
                        operation = "EDIT_REQUEST"
                        strDescription = "<b>The below request IDs have been created from this ticket:" & vbCrLf & recordedids & "</b>" & vbCrLf & vbCrLf & drOpen("DESCRIPTION")

                        strXML = ""

                        strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                        strXML &= "<Operation>"
                        strXML &= "<Details>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Description</name>"
                        strXML &= "<value>" & Replace(strDescription, "&", "and") & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "</Details>"
                        strXML &= "</Operation>"

                        outcome = PostTo(strHD, strXML)

                        If outcome.StartsWith("Success: ") Then

                            strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL() & drOpen("WORKORDERID") & "/"
                            operation = "CLOSE_REQUEST"

                            strXML = ""

                            strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                            strXML &= "<Operation>"
                            strXML &= "<Details>"

                            strXML &= "<parameter>"
                            strXML &= "<name>closeAccepted</name>"
                            strXML &= "<value>Accepted</value>"
                            strXML &= "</parameter>"

                            strXML &= "</Details>"
                            strXML &= "</Operation>"


                            outcome = PostTo(strHD, strXML)

                            If outcome.StartsWith("Success: ") Then
                                Console.WriteLine(Now & " | Multi Country Budget Requests - Finished Request (" & drOpen("WORKORDERID") & ")")
                            Else
                                SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Country Budget Request Import | " & drOpen("WORKORDERID"), "Closing Request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")

                            End If
                        Else
                            SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Country Budget Request Import | " & drOpen("WORKORDERID"), "Updating Request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")
                        End If

                    End If

                Next

            End If




        Catch ex As Exception
            Console.WriteLine(Now & " | Multi Country Budget Requests - " & ex.Message)
        End Try
    End Sub

    Private Sub Process_BudgetRequests_MultiClient()
        Try

            ' Get a list of all "OPEN" multi-country requests

            Dim strOpen As String = "USE [servicedesk] "
            strOpen &= "SELECT "
            strOpen &= "a.WORKORDERID"
            strOpen &= ", a.REQUESTERID "
            strOpen &= ", a.CREATEDBYID "
            strOpen &= ", a.TITLE "
            strOpen &= ", a.DESCRIPTION "
            strOpen &= ", c.STATUSID "
            strOpen &= ", c.OWNERID "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= a.CREATEDBYID)		[creator] "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= a.REQUESTERID)		[requester] "
            strOpen &= ", (SELECT FIRST_NAME FROM AAAUSER WHERE USER_ID= c.OWNERID)			[technician] "
            strOpen &= ", d.GUDF_CHAR1 [customer]"
            strOpen &= ", CASE WHEN LEN(CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR)) = 1 THEN '0' + CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR) ELSE CAST(DATEPART(Day, [se].[fn_GetDateTime](UDF_DATE2)) AS VARCHAR) END + ' ' + LEFT(datename(month, [se].[fn_GetDateTime](UDF_DATE2)), 3) + ' ' + cast(datepart(year, [se].[fn_GetDateTime](UDF_DATE2)) as varchar) [duedate] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE1) [date1] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE2) [date2] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE3) [date3] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE4) [date4] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE5) [date5] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE6) [date6] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE7) [date7] "
            strOpen &= ", [se].[fn_GetDateTime](UDF_DATE8) [date8] "
            strOpen &= ", [se].[fn_GetDateTime](a.CREATEDTIME) [createddate] "
            strOpen &= ", b.* "
            strOpen &= "FROM "
            strOpen &= "[dbo].[WorkOrder] a "
            strOpen &= "INNER JOIN [dbo].[ServiceReq_5101] b on a.WORKORDERID = b.WORKORDERID "
            strOpen &= "INNER JOIN [dbo].[workorderstates] c on a.workorderid = c.workorderid "
            strOpen &= "INNER JOIN [dbo].[servicecatalog_fields] d ON a.WORKORDERID = d.WORKORDERID "
            strOpen &= "WHERE a.TEMPLATEID = 13201 " 'This template is the multi country id
            strOpen &= "AND a.COMPLETEDTIME = 0 " ' basically not already completed
            strOpen &= "AND a.WORKORDERID NOT IN (53372) " 'dodgy muppet who didnt specify the customer

            Dim dtOpen As New DataTable
            dtOpen = data_select(strOpen, fnConnectionStringHD)

            Dim strHD As String = fnHelpdeskURL() & fnHelpdeskRESTAPIURL()
            Dim operation As String = ""

            Dim strDescription As String = ""
            Dim reqtemplate As String = ""
            Dim strRequester As String = ""
            Dim strSite As String = ""
            Dim strGroup As String = ""
            Dim strCountry As String = ""
            Dim strCustomer As String = ""
            Dim strCommodity As String = ""
            Dim strDueDate As String = ""
            Dim strTitle As String = ""

            Dim outcome As String = ""


            Dim strXML As String = ""

            If dtOpen.Rows.Count > 0 Then

                Console.WriteLine(Now & " | Multi Client Budget Requests - Start (" & dtOpen.Rows.Count & ")")

                For Each drOpen As DataRow In dtOpen.Rows

                    Dim recordedids As String = ""

                    Dim strClientList As String = drOpen("UDF_CHAR13")
                    Dim strClients As String() = strClientList.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    'For Each client As String In strClients
                    '    MsgBox(client)
                    'Next

                    Console.WriteLine(Now & " | RequestID: " & drOpen("WORKORDERID") & " | " & strClients.Length & " clients")
                    If strClients.Length > 0 Then
                        For Each client As String In strClients

                            Dim strCountries As String = "USE [servicedesk] "
                            strCountries &= "select right(options, (len(options)-(CHARINDEX('|',options))-1)) [country]  "
                            strCountries &= "from Question_Options where QUESTIONID = 3901 "
                            strCountries &= "and left(options, (CHARINDEX('|',options)-2)) = '" & drOpen("UDF_CHAR10") & "'"


                            strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL()
                            operation = "ADD_REQUEST"
                            strDescription = "This requests was automatically created from the Multi Client request ID: " & drOpen("WORKORDERID") & vbCrLf & vbCrLf & drOpen("DESCRIPTION")
                            reqtemplate = "Budget Requests"
                            strRequester = drOpen("requester")
                            strSite = "Budget Requests"
                            If drOpen("UDF_CHAR14") = "No" Then 'default group
                                strGroup = "* Default Group"
                            Else 'country specific
                                strGroup = data_select_value(strCountries, fnConnectionStringHD)
                            End If
                            strCountry = drOpen("UDF_CHAR10")
                            strCustomer = client
                            strCommodity = drOpen("UDF_CHAR5")
                            strDueDate = drOpen("duedate")
                            strTitle = "Budget Request - " & strCustomer & " | " & strCountry & " | " & strCommodity & " | " & strDueDate


                            strXML = ""

                            strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                            strXML &= "<Operation>"
                            strXML &= "<Details>"

                            strXML &= "<parameter>"
                            strXML &= "<name>requesttemplate</name>"
                            strXML &= "<value>" & reqtemplate & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>requester</name>"
                            strXML &= "<value>" & strRequester & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>site</name>"
                            strXML &= "<value>" & strSite & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>group</name>"
                            strXML &= "<value>" & strGroup & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Subject</name>"
                            strXML &= "<value>" & strTitle & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Description</name>"
                            strXML &= "<value>" & Replace(strDescription, "&", "and") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Customer</name>"
                            strXML &= "<value>" & strCustomer & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>CBMS Budget ID</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR1") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Other Comments</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR2") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Query Contact</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR3") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Completion Contact</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR4") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Commodity</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR5") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Commodity Methodology</name>"
                            strXML &= "<value>" & Replace(drOpen("UDF_CHAR6"), "+", "and") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Annual Budgeting Service Only</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR14") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Consumption Methodology</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR7") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Reforecast?</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR8") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Reviewed  By</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR9") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Country Code</name>"
                            strXML &= "<value>" & strCountry & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Annual Budgeting Service Only</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR14") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>Requestor OLA Breach</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR11") & "</value>"
                            strXML &= "</parameter>"

                            strXML &= "<parameter>"
                            strXML &= "<name>CBMS Budget?</name>"
                            strXML &= "<value>" & drOpen("UDF_CHAR12") & "</value>"
                            strXML &= "</parameter>"

                            If Not IsDBNull(drOpen("date1")) Then

                                Dim date1 As Date = drOpen("date1")
                                Dim date1_string As String = date1.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Client Inputs Finalised Date</name>"
                                strXML &= "<value>" & date1_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date2")) Then

                                Dim date2 As Date = drOpen("date2")
                                Dim date2_string As String = date2.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Desired Due Date</name>"
                                strXML &= "<value>" & date2_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date3")) Then


                                Dim date3 As Date = drOpen("date3")
                                Dim date3_string As String = date3.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Budget From Date</name>"
                                strXML &= "<value>" & date3_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date4")) Then

                                Dim date4 As Date = drOpen("date4")
                                Dim date4_string As String = date4.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Missing Inputs Acquired Date</name>"
                                strXML &= "<value>" & date4_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date5")) Then

                                Dim date5 As Date = drOpen("date5")
                                Dim date5_string As String = date5.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Desired Price Date</name>"
                                strXML &= "<value>" & date5_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date6")) Then

                                Dim date6 As Date = drOpen("date6")
                                Dim date6_string As String = date6.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Assumptions Agreed Date</name>"
                                strXML &= "<value>" & date6_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date7")) Then

                                Dim date7 As Date = drOpen("date7")
                                Dim date7_string As String = date7.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Costs Modelled Date</name>"
                                strXML &= "<value>" & date7_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            If Not IsDBNull(drOpen("date8")) Then

                                Dim date8 As Date = drOpen("date8")
                                Dim date8_string As String = date8.ToString("dd MMM yyyy, HH:mm:ss")

                                strXML &= "<parameter>"
                                strXML &= "<name>Internal Review Date</name>"
                                strXML &= "<value>" & date8_string & "</value>"
                                strXML &= "</parameter>"

                            End If

                            strXML &= "</Details>"
                            strXML &= "</Operation>"

                            outcome = PostTo(strHD, strXML)

                            If outcome.StartsWith("Success: ") Then
                                recordedids &= Replace(outcome, "Success", client) & vbCrLf
                                RecordLogRecord("Multi Country Budget Requests", outcome, "Success", 1)
                            Else
                                SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Client Budget Request Import | " & drOpen("WORKORDERID"), "Adding " & client & " request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")
                            End If

                        Next

                        ' this point all the NEW requests are done.. 
                        ' update the original to include all the child id's in the desc
                        ' close down the original multi country request 

                        strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL() & drOpen("WORKORDERID") & "/"
                        operation = "EDIT_REQUEST"
                        strDescription = "<b>The below request IDs have been created from this ticket:" & vbCrLf & recordedids & "</b>" & vbCrLf & vbCrLf & drOpen("DESCRIPTION")

                        strXML = ""

                        strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                        strXML &= "<Operation>"
                        strXML &= "<Details>"

                        strXML &= "<parameter>"
                        strXML &= "<name>Description</name>"
                        strXML &= "<value>" & Replace(strDescription, "&", "and") & "</value>"
                        strXML &= "</parameter>"

                        strXML &= "</Details>"
                        strXML &= "</Operation>"

                        outcome = PostTo(strHD, strXML)

                        If outcome.StartsWith("Success: ") Then

                            strHD = fnHelpdeskURL() & fnHelpdeskRESTAPIURL() & drOpen("WORKORDERID") & "/"
                            operation = "CLOSE_REQUEST"

                            strXML = ""

                            strXML &= "?OPERATION_NAME=" & operation & "&TECHNICIAN_KEY=" & fnHelpdeskRESTAPIkey() & "&INPUT_DATA="
                            strXML &= "<Operation>"
                            strXML &= "<Details>"

                            strXML &= "<parameter>"
                            strXML &= "<name>closeAccepted</name>"
                            strXML &= "<value>Accepted</value>"
                            strXML &= "</parameter>"

                            strXML &= "</Details>"
                            strXML &= "</Operation>"


                            outcome = PostTo(strHD, strXML)

                            If outcome.StartsWith("Success: ") Then
                                Console.WriteLine(Now & " | Multi Client Budget Requests - Finished Request (" & drOpen("WORKORDERID") & ")")
                            Else
                                SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Client Budget Request Import | " & drOpen("WORKORDERID"), "Closing Request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")

                            End If
                        Else
                            SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Multi Client Budget Request Import | " & drOpen("WORKORDERID"), "Updating Request" & vbCrLf & vbCrLf & outcome, "", "HTML", "", "", "")
                        End If

                    End If

                Next

            End If




        Catch ex As Exception
            Console.WriteLine(Now & " | Multi Client Budget Requests - " & ex.Message)
        End Try
    End Sub

    Private Sub ProcessFTPCarillionFiles()

        Dim host As String = My.Settings.FTP_carillion_url
        Dim username As String = My.Settings.FTP_carillion_username
        Dim password As String = My.Settings.FTP_carillion_password
        Dim stLocalDir As String = Core.fnHalfHourDataDirectory & "Carillion FTP\"

        'Create a link to an FtpServer
        Dim ftp As New clsFTPClient(host, username, password)

        Dim strDate As String = ""

        'Get the detailed directory listing of /pub/
        Dim dirList As FTPdirectory = ftp.ListDirectoryDetail("/")

        'filter out only the files in the list
        Dim filesOnly As FTPdirectory = dirList.GetFiles("csv")
        'download these files
        Dim x As Integer = 0
        If filesOnly.Count > 0 Then : x = 1 : End If

        For Each file As FTPfileInfo In filesOnly
            strDate = Replace(Replace(Replace(Date.Now(), "/", ""), " ", "_"), ":", "")
            'Console.WriteLine(file.Filename)

            ftp.Download(file, stLocalDir & file.Filename, True)
            ftp.FtpRename(file.Filename, "Processed CSV/" & file.Filename)
            RecordLogRecord("FTP: Carillion", stLocalDir & file.Filename, "Success", 1)
            'RecordLogRecord()
        Next

        Dim xmlfilesOnly As FTPdirectory = dirList.GetFiles("mmxml")
        'download these files
        Dim xmlx As Integer = 0
        If xmlfilesOnly.Count > 0 Then : xmlx = 1 : End If

        For Each file As FTPfileInfo In xmlfilesOnly
            'Console.WriteLine(file.Filename)
            ftp.FtpRename(file.Filename, "Processed XML/" & file.Filename)

        Next

        If xmlx = 1 Then : xmlx = 0 : End If

    End Sub

    Private Sub ProcessFTPCarillionRecords()

        Dim var_foldername As String = My.Settings.FTP_carillion_foldername

        ListBox1.Items.Clear()

        ' make a reference to a directory
        Dim di As New IO.DirectoryInfo(Core.fnHalfHourDataDirectory & var_foldername & "\") ' source
        Dim diar1 As IO.FileInfo() = di.GetFiles("*.csv") 'file type
        Dim dra As IO.FileInfo
        Dim destination As String = ""
        Dim strOutput As String = ""

        'list the names of all files in the specified directory
        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Carillion FTP - Files Start (" & diar1.Length & " files)") : End If
        For Each dra In diar1
            Try

                If IsFileOpen(dra.Directory.ToString & "\" & dra.ToString) = False Then

                    Dim strSQL As String = "EXEC UML_EXTData.dbo.usp_HHDataImport_ImportCarillionFTP '" & dra.ToString & "'"
                    strOutput = data_select_value(strSQL, fnConnectionString)

                    OutputFromSQL("Files: Carillion FTP", strOutput, dra.ToString, dra.DirectoryName.ToString)

                    Console.WriteLine(Now & " | Files: Carillion FTP - " & strOutput)

                Else
                    Dim strError As String = "In file: " & dra.ToString & vbCrLf & "Error Details: " & "The file is already open.  The system will try and import this file on the next pass."
                    SendSMTP("donotreply@ems.schneider-electric.com", "dave.clarke@ems.schneider-electric.com", "Carillion FTP Import Error", strError, "")
                End If
            Catch ex As Exception
                If ex.Message.StartsWith("") Then

                End If
                Console.WriteLine(Now & " | Carillion FTP File Error | " & ex.Message)
            End Try

        Next

        'If diar1.Length > 0 Then : Console.WriteLine(Now & " | Carillion FTP - Files End") : End If

    End Sub


End Class
