<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.chkFTPTruReadElecFiles = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.intRecordsImported = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.varApplicationName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAllFiles = New System.Windows.Forms.Button()
        Me.chkIMServEmails = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkBudgetRequests_MultiClient = New System.Windows.Forms.CheckBox()
        Me.chkBudgetRequests_MultiCountry = New System.Windows.Forms.CheckBox()
        Me.chkSEMOXMLFiles = New System.Windows.Forms.CheckBox()
        Me.chkAutomatedReports = New System.Windows.Forms.CheckBox()
        Me.chkUKFixedTerminationRequest = New System.Windows.Forms.CheckBox()
        Me.chkTenantStatments = New System.Windows.Forms.CheckBox()
        Me.chkFilesStark = New System.Windows.Forms.CheckBox()
        Me.chkEmailStark = New System.Windows.Forms.CheckBox()
        Me.chkTerminations = New System.Windows.Forms.CheckBox()
        Me.chkDataServices = New System.Windows.Forms.CheckBox()
        Me.chkCarillionaMT = New System.Windows.Forms.CheckBox()
        Me.chkFTPTruReadElecRecords = New System.Windows.Forms.CheckBox()
        Me.chkFTPGazpromElecFiles = New System.Windows.Forms.CheckBox()
        Me.chkTrend = New System.Windows.Forms.CheckBox()
        Me.chkIMServ = New System.Windows.Forms.CheckBox()
        Me.chkRTUZ = New System.Windows.Forms.CheckBox()
        Me.chkFTPGazpromElecRecords = New System.Windows.Forms.CheckBox()
        Me.chkPhillips = New System.Windows.Forms.CheckBox()
        Me.chkGeneric = New System.Windows.Forms.CheckBox()
        Me.chkRTUZFiles = New System.Windows.Forms.CheckBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblCountdown = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTimerInterval = New System.Windows.Forms.TextBox()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.prgTimer = New System.Windows.Forms.ProgressBar()
        Me.time_Progress = New System.Windows.Forms.Timer(Me.components)
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.btnProcessSwitch = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblcontact = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.chkFTPCarillion = New System.Windows.Forms.CheckBox()
        Me.chkFilesCarillionFTP = New System.Windows.Forms.CheckBox()
        Me.GroupBox2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(644, 442)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 21
        Me.TextBox1.Visible = False
        '
        'chkFTPTruReadElecFiles
        '
        Me.chkFTPTruReadElecFiles.Checked = True
        Me.chkFTPTruReadElecFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFTPTruReadElecFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFTPTruReadElecFiles.Location = New System.Drawing.Point(18, 36)
        Me.chkFTPTruReadElecFiles.Name = "chkFTPTruReadElecFiles"
        Me.chkFTPTruReadElecFiles.Size = New System.Drawing.Size(118, 17)
        Me.chkFTPTruReadElecFiles.TabIndex = 14
        Me.chkFTPTruReadElecFiles.Text = "FTP: TruRead Gas"
        Me.chkFTPTruReadElecFiles.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(750, 437)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(63, 25)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Start Processing"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'intRecordsImported
        '
        Me.intRecordsImported.Text = "Records Today"
        Me.intRecordsImported.Width = 171
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.Location = New System.Drawing.Point(1175, 12)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(470, 212)
        Me.ListBox3.TabIndex = 19
        Me.ListBox3.Visible = False
        '
        'varApplicationName
        '
        Me.varApplicationName.Text = "Source"
        Me.varApplicationName.Width = 252
        '
        'btnAllFiles
        '
        Me.btnAllFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnAllFiles.ForeColor = System.Drawing.Color.White
        Me.btnAllFiles.Location = New System.Drawing.Point(490, 65)
        Me.btnAllFiles.Name = "btnAllFiles"
        Me.btnAllFiles.Size = New System.Drawing.Size(87, 215)
        Me.btnAllFiles.TabIndex = 18
        Me.btnAllFiles.Text = "untick all boxes"
        Me.btnAllFiles.UseVisualStyleBackColor = False
        '
        'chkIMServEmails
        '
        Me.chkIMServEmails.Checked = True
        Me.chkIMServEmails.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIMServEmails.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIMServEmails.Location = New System.Drawing.Point(18, 87)
        Me.chkIMServEmails.Name = "chkIMServEmails"
        Me.chkIMServEmails.Size = New System.Drawing.Size(118, 17)
        Me.chkIMServEmails.TabIndex = 15
        Me.chkIMServEmails.Text = "Emails: IMServ"
        Me.chkIMServEmails.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.chkFilesCarillionFTP)
        Me.GroupBox2.Controls.Add(Me.chkFTPCarillion)
        Me.GroupBox2.Controls.Add(Me.chkBudgetRequests_MultiClient)
        Me.GroupBox2.Controls.Add(Me.chkBudgetRequests_MultiCountry)
        Me.GroupBox2.Controls.Add(Me.chkSEMOXMLFiles)
        Me.GroupBox2.Controls.Add(Me.chkAutomatedReports)
        Me.GroupBox2.Controls.Add(Me.chkUKFixedTerminationRequest)
        Me.GroupBox2.Controls.Add(Me.chkTenantStatments)
        Me.GroupBox2.Controls.Add(Me.chkFilesStark)
        Me.GroupBox2.Controls.Add(Me.chkEmailStark)
        Me.GroupBox2.Controls.Add(Me.chkTerminations)
        Me.GroupBox2.Controls.Add(Me.chkDataServices)
        Me.GroupBox2.Controls.Add(Me.chkIMServEmails)
        Me.GroupBox2.Controls.Add(Me.chkCarillionaMT)
        Me.GroupBox2.Controls.Add(Me.chkFTPTruReadElecFiles)
        Me.GroupBox2.Controls.Add(Me.chkFTPTruReadElecRecords)
        Me.GroupBox2.Controls.Add(Me.chkFTPGazpromElecFiles)
        Me.GroupBox2.Controls.Add(Me.chkTrend)
        Me.GroupBox2.Controls.Add(Me.chkIMServ)
        Me.GroupBox2.Controls.Add(Me.chkRTUZ)
        Me.GroupBox2.Controls.Add(Me.chkFTPGazpromElecRecords)
        Me.GroupBox2.Controls.Add(Me.chkPhillips)
        Me.GroupBox2.Controls.Add(Me.chkGeneric)
        Me.GroupBox2.Controls.Add(Me.chkRTUZFiles)
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(12, 65)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(474, 215)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Files to Import"
        '
        'chkBudgetRequests_MultiClient
        '
        Me.chkBudgetRequests_MultiClient.Checked = True
        Me.chkBudgetRequests_MultiClient.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBudgetRequests_MultiClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBudgetRequests_MultiClient.Location = New System.Drawing.Point(313, 155)
        Me.chkBudgetRequests_MultiClient.Name = "chkBudgetRequests_MultiClient"
        Me.chkBudgetRequests_MultiClient.Size = New System.Drawing.Size(155, 17)
        Me.chkBudgetRequests_MultiClient.TabIndex = 26
        Me.chkBudgetRequests_MultiClient.Text = "HD: Multi Client Budgets"
        Me.chkBudgetRequests_MultiClient.UseVisualStyleBackColor = True
        '
        'chkBudgetRequests_MultiCountry
        '
        Me.chkBudgetRequests_MultiCountry.Checked = True
        Me.chkBudgetRequests_MultiCountry.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBudgetRequests_MultiCountry.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBudgetRequests_MultiCountry.Location = New System.Drawing.Point(313, 138)
        Me.chkBudgetRequests_MultiCountry.Name = "chkBudgetRequests_MultiCountry"
        Me.chkBudgetRequests_MultiCountry.Size = New System.Drawing.Size(155, 17)
        Me.chkBudgetRequests_MultiCountry.TabIndex = 25
        Me.chkBudgetRequests_MultiCountry.Text = "HD: Multi Country Budgets"
        Me.chkBudgetRequests_MultiCountry.UseVisualStyleBackColor = True
        '
        'chkSEMOXMLFiles
        '
        Me.chkSEMOXMLFiles.Checked = True
        Me.chkSEMOXMLFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSEMOXMLFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSEMOXMLFiles.Location = New System.Drawing.Point(313, 104)
        Me.chkSEMOXMLFiles.Name = "chkSEMOXMLFiles"
        Me.chkSEMOXMLFiles.Size = New System.Drawing.Size(155, 17)
        Me.chkSEMOXMLFiles.TabIndex = 24
        Me.chkSEMOXMLFiles.Text = "XML: SEMO Files"
        Me.chkSEMOXMLFiles.UseVisualStyleBackColor = True
        '
        'chkAutomatedReports
        '
        Me.chkAutomatedReports.Checked = True
        Me.chkAutomatedReports.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutomatedReports.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAutomatedReports.Location = New System.Drawing.Point(313, 70)
        Me.chkAutomatedReports.Name = "chkAutomatedReports"
        Me.chkAutomatedReports.Size = New System.Drawing.Size(155, 17)
        Me.chkAutomatedReports.TabIndex = 23
        Me.chkAutomatedReports.Text = "Report: Automated Reports"
        Me.chkAutomatedReports.UseVisualStyleBackColor = True
        '
        'chkUKFixedTerminationRequest
        '
        Me.chkUKFixedTerminationRequest.Checked = True
        Me.chkUKFixedTerminationRequest.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUKFixedTerminationRequest.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUKFixedTerminationRequest.Location = New System.Drawing.Point(18, 138)
        Me.chkUKFixedTerminationRequest.Name = "chkUKFixedTerminationRequest"
        Me.chkUKFixedTerminationRequest.Size = New System.Drawing.Size(118, 17)
        Me.chkUKFixedTerminationRequest.TabIndex = 22
        Me.chkUKFixedTerminationRequest.Text = "Emails: UK Fix Term"
        Me.chkUKFixedTerminationRequest.UseVisualStyleBackColor = True
        '
        'chkTenantStatments
        '
        Me.chkTenantStatments.Checked = True
        Me.chkTenantStatments.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTenantStatments.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTenantStatments.Location = New System.Drawing.Point(313, 53)
        Me.chkTenantStatments.Name = "chkTenantStatments"
        Me.chkTenantStatments.Size = New System.Drawing.Size(145, 17)
        Me.chkTenantStatments.TabIndex = 21
        Me.chkTenantStatments.Text = "Report: Tenant Statments"
        Me.chkTenantStatments.UseVisualStyleBackColor = True
        '
        'chkFilesStark
        '
        Me.chkFilesStark.Checked = True
        Me.chkFilesStark.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFilesStark.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFilesStark.Location = New System.Drawing.Point(165, 87)
        Me.chkFilesStark.Name = "chkFilesStark"
        Me.chkFilesStark.Size = New System.Drawing.Size(119, 17)
        Me.chkFilesStark.TabIndex = 20
        Me.chkFilesStark.Text = "Files: Stark"
        Me.chkFilesStark.UseVisualStyleBackColor = True
        '
        'chkEmailStark
        '
        Me.chkEmailStark.Checked = True
        Me.chkEmailStark.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEmailStark.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmailStark.Location = New System.Drawing.Point(18, 104)
        Me.chkEmailStark.Name = "chkEmailStark"
        Me.chkEmailStark.Size = New System.Drawing.Size(118, 17)
        Me.chkEmailStark.TabIndex = 19
        Me.chkEmailStark.Text = "Emails: Stark"
        Me.chkEmailStark.UseVisualStyleBackColor = True
        '
        'chkTerminations
        '
        Me.chkTerminations.Checked = True
        Me.chkTerminations.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTerminations.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTerminations.Location = New System.Drawing.Point(313, 36)
        Me.chkTerminations.Name = "chkTerminations"
        Me.chkTerminations.Size = New System.Drawing.Size(145, 17)
        Me.chkTerminations.TabIndex = 18
        Me.chkTerminations.Text = "Report: Terminations"
        Me.chkTerminations.UseVisualStyleBackColor = True
        '
        'chkDataServices
        '
        Me.chkDataServices.Checked = True
        Me.chkDataServices.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDataServices.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDataServices.Location = New System.Drawing.Point(313, 19)
        Me.chkDataServices.Name = "chkDataServices"
        Me.chkDataServices.Size = New System.Drawing.Size(145, 17)
        Me.chkDataServices.TabIndex = 17
        Me.chkDataServices.Text = "Report: Data Services"
        Me.chkDataServices.UseVisualStyleBackColor = True
        '
        'chkCarillionaMT
        '
        Me.chkCarillionaMT.Checked = True
        Me.chkCarillionaMT.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCarillionaMT.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCarillionaMT.Location = New System.Drawing.Point(165, 155)
        Me.chkCarillionaMT.Name = "chkCarillionaMT"
        Me.chkCarillionaMT.Size = New System.Drawing.Size(119, 17)
        Me.chkCarillionaMT.TabIndex = 16
        Me.chkCarillionaMT.Text = "Files: Carillion"
        Me.chkCarillionaMT.UseVisualStyleBackColor = True
        '
        'chkFTPTruReadElecRecords
        '
        Me.chkFTPTruReadElecRecords.Checked = True
        Me.chkFTPTruReadElecRecords.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFTPTruReadElecRecords.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkFTPTruReadElecRecords.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red
        Me.chkFTPTruReadElecRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFTPTruReadElecRecords.Location = New System.Drawing.Point(165, 36)
        Me.chkFTPTruReadElecRecords.Name = "chkFTPTruReadElecRecords"
        Me.chkFTPTruReadElecRecords.Size = New System.Drawing.Size(119, 17)
        Me.chkFTPTruReadElecRecords.TabIndex = 15
        Me.chkFTPTruReadElecRecords.Text = "Files: TruRead Gas"
        Me.chkFTPTruReadElecRecords.UseVisualStyleBackColor = True
        '
        'chkFTPGazpromElecFiles
        '
        Me.chkFTPGazpromElecFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFTPGazpromElecFiles.Location = New System.Drawing.Point(18, 19)
        Me.chkFTPGazpromElecFiles.Name = "chkFTPGazpromElecFiles"
        Me.chkFTPGazpromElecFiles.Size = New System.Drawing.Size(118, 17)
        Me.chkFTPGazpromElecFiles.TabIndex = 12
        Me.chkFTPGazpromElecFiles.Text = "FTP: Gazprom Elec"
        Me.chkFTPGazpromElecFiles.UseVisualStyleBackColor = True
        '
        'chkTrend
        '
        Me.chkTrend.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTrend.Location = New System.Drawing.Point(165, 121)
        Me.chkTrend.Name = "chkTrend"
        Me.chkTrend.Size = New System.Drawing.Size(119, 17)
        Me.chkTrend.TabIndex = 5
        Me.chkTrend.Text = "Files: Trend BMS"
        Me.chkTrend.UseVisualStyleBackColor = True
        '
        'chkIMServ
        '
        Me.chkIMServ.Checked = True
        Me.chkIMServ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIMServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIMServ.Location = New System.Drawing.Point(165, 70)
        Me.chkIMServ.Name = "chkIMServ"
        Me.chkIMServ.Size = New System.Drawing.Size(119, 17)
        Me.chkIMServ.TabIndex = 7
        Me.chkIMServ.Text = "Files: IMServ"
        Me.chkIMServ.UseVisualStyleBackColor = True
        '
        'chkRTUZ
        '
        Me.chkRTUZ.Checked = True
        Me.chkRTUZ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRTUZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRTUZ.Location = New System.Drawing.Point(18, 70)
        Me.chkRTUZ.Name = "chkRTUZ"
        Me.chkRTUZ.Size = New System.Drawing.Size(118, 17)
        Me.chkRTUZ.TabIndex = 9
        Me.chkRTUZ.Text = "Emails: RTU Z "
        Me.chkRTUZ.UseVisualStyleBackColor = True
        '
        'chkFTPGazpromElecRecords
        '
        Me.chkFTPGazpromElecRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFTPGazpromElecRecords.Location = New System.Drawing.Point(165, 19)
        Me.chkFTPGazpromElecRecords.Name = "chkFTPGazpromElecRecords"
        Me.chkFTPGazpromElecRecords.Size = New System.Drawing.Size(119, 17)
        Me.chkFTPGazpromElecRecords.TabIndex = 13
        Me.chkFTPGazpromElecRecords.Text = "Files: Gazprom Elec"
        Me.chkFTPGazpromElecRecords.UseVisualStyleBackColor = True
        '
        'chkPhillips
        '
        Me.chkPhillips.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPhillips.Location = New System.Drawing.Point(165, 104)
        Me.chkPhillips.Name = "chkPhillips"
        Me.chkPhillips.Size = New System.Drawing.Size(119, 17)
        Me.chkPhillips.TabIndex = 6
        Me.chkPhillips.Text = "Files: Phillips Lighting"
        Me.chkPhillips.UseVisualStyleBackColor = True
        '
        'chkGeneric
        '
        Me.chkGeneric.Checked = True
        Me.chkGeneric.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGeneric.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGeneric.Location = New System.Drawing.Point(165, 138)
        Me.chkGeneric.Name = "chkGeneric"
        Me.chkGeneric.Size = New System.Drawing.Size(119, 17)
        Me.chkGeneric.TabIndex = 8
        Me.chkGeneric.Text = "Files: Generic"
        Me.chkGeneric.UseVisualStyleBackColor = True
        '
        'chkRTUZFiles
        '
        Me.chkRTUZFiles.Checked = True
        Me.chkRTUZFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRTUZFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRTUZFiles.Location = New System.Drawing.Point(165, 53)
        Me.chkRTUZFiles.Name = "chkRTUZFiles"
        Me.chkRTUZFiles.Size = New System.Drawing.Size(119, 17)
        Me.chkRTUZFiles.TabIndex = 10
        Me.chkRTUZFiles.Text = "Files: RTU Z "
        Me.chkRTUZFiles.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 513)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(587, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(16, 17)
        Me.ToolStripStatusLabel1.Text = "..."
        '
        'lblCountdown
        '
        Me.lblCountdown.AutoSize = True
        Me.lblCountdown.Location = New System.Drawing.Point(555, 437)
        Me.lblCountdown.Name = "lblCountdown"
        Me.lblCountdown.Size = New System.Drawing.Size(22, 13)
        Me.lblCountdown.TabIndex = 14
        Me.lblCountdown.Text = "....."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(51, 437)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 13)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "This is the timer interval (minutes)"
        '
        'txtTimerInterval
        '
        Me.txtTimerInterval.Location = New System.Drawing.Point(10, 434)
        Me.txtTimerInterval.Name = "txtTimerInterval"
        Me.txtTimerInterval.Size = New System.Drawing.Size(35, 20)
        Me.txtTimerInterval.TabIndex = 12
        Me.txtTimerInterval.Text = "5"
        '
        'ListView1
        '
        Me.ListView1.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.ListView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.varApplicationName, Me.intRecordsImported})
        Me.ListView1.ForeColor = System.Drawing.Color.White
        Me.ListView1.Location = New System.Drawing.Point(12, 286)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(567, 138)
        Me.ListView1.TabIndex = 11
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'prgTimer
        '
        Me.prgTimer.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.prgTimer.ForeColor = System.Drawing.Color.White
        Me.prgTimer.Location = New System.Drawing.Point(10, 459)
        Me.prgTimer.Name = "prgTimer"
        Me.prgTimer.Size = New System.Drawing.Size(567, 17)
        Me.prgTimer.TabIndex = 3
        '
        'time_Progress
        '
        Me.time_Progress.Interval = 1000
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(1175, 435)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(212, 17)
        Me.ListBox1.TabIndex = 0
        Me.ListBox1.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(1175, 230)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(470, 199)
        Me.ListBox2.TabIndex = 2
        Me.ListBox2.Visible = False
        '
        'btnProcessSwitch
        '
        Me.btnProcessSwitch.BackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.btnProcessSwitch.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnProcessSwitch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.btnProcessSwitch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.btnProcessSwitch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(159, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.btnProcessSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnProcessSwitch.Location = New System.Drawing.Point(10, 480)
        Me.btnProcessSwitch.Name = "btnProcessSwitch"
        Me.btnProcessSwitch.Size = New System.Drawing.Size(567, 25)
        Me.btnProcessSwitch.TabIndex = 1
        Me.btnProcessSwitch.Text = "Start Processing"
        Me.btnProcessSwitch.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(598, 65)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(556, 121)
        Me.DataGridView1.TabIndex = 4
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.HHDataApp.My.Resources.Resources.Schneider_Logo___White_on_Green___Jpg
        Me.PictureBox1.Location = New System.Drawing.Point(427, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(158, 58)
        Me.PictureBox1.TabIndex = 22
        Me.PictureBox1.TabStop = False
        '
        'lblcontact
        '
        Me.lblcontact.AutoSize = True
        Me.lblcontact.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.lblcontact.ForeColor = System.Drawing.Color.White
        Me.lblcontact.Location = New System.Drawing.Point(12, 48)
        Me.lblcontact.Name = "lblcontact"
        Me.lblcontact.Size = New System.Drawing.Size(393, 13)
        Me.lblcontact.TabIndex = 23
        Me.lblcontact.Text = "Please contact dave.clarke@ems.schneider-electric.com for queries"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(12, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(386, 23)
        Me.lblTitle.TabIndex = 24
        Me.lblTitle.Text = "Schneider Electric Interval Data Loader"
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummary.ForeColor = System.Drawing.Color.White
        Me.lblSummary.Location = New System.Drawing.Point(13, 30)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(19, 13)
        Me.lblSummary.TabIndex = 25
        Me.lblSummary.Text = "..."
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(598, 192)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(556, 121)
        Me.DataGridView2.TabIndex = 26
        '
        'chkFTPCarillion
        '
        Me.chkFTPCarillion.Checked = True
        Me.chkFTPCarillion.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFTPCarillion.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFTPCarillion.Location = New System.Drawing.Point(18, 53)
        Me.chkFTPCarillion.Name = "chkFTPCarillion"
        Me.chkFTPCarillion.Size = New System.Drawing.Size(118, 17)
        Me.chkFTPCarillion.TabIndex = 27
        Me.chkFTPCarillion.Text = "FTP: Carillion"
        Me.chkFTPCarillion.UseVisualStyleBackColor = True
        '
        'chkFilesCarillionFTP
        '
        Me.chkFilesCarillionFTP.Checked = True
        Me.chkFilesCarillionFTP.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFilesCarillionFTP.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFilesCarillionFTP.Location = New System.Drawing.Point(165, 170)
        Me.chkFilesCarillionFTP.Name = "chkFilesCarillionFTP"
        Me.chkFilesCarillionFTP.Size = New System.Drawing.Size(119, 17)
        Me.chkFilesCarillionFTP.TabIndex = 28
        Me.chkFilesCarillionFTP.Text = "Files: Carillion FTP"
        Me.chkFilesCarillionFTP.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(587, 535)
        Me.Controls.Add(Me.DataGridView2)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lblcontact)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.btnAllFiles)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblCountdown)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTimerInterval)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.prgTimer)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.btnProcessSwitch)
        Me.Controls.Add(Me.DataGridView1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.TopMost = True
        Me.GroupBox2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents chkFTPTruReadElecFiles As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents intRecordsImported As System.Windows.Forms.ColumnHeader
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents varApplicationName As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAllFiles As System.Windows.Forms.Button
    Friend WithEvents chkIMServEmails As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkCarillionaMT As System.Windows.Forms.CheckBox
    Friend WithEvents chkFTPTruReadElecRecords As System.Windows.Forms.CheckBox
    Friend WithEvents chkFTPGazpromElecFiles As System.Windows.Forms.CheckBox
    Friend WithEvents chkTrend As System.Windows.Forms.CheckBox
    Friend WithEvents chkIMServ As System.Windows.Forms.CheckBox
    Friend WithEvents chkRTUZ As System.Windows.Forms.CheckBox
    Friend WithEvents chkFTPGazpromElecRecords As System.Windows.Forms.CheckBox
    Friend WithEvents chkPhillips As System.Windows.Forms.CheckBox
    Friend WithEvents chkGeneric As System.Windows.Forms.CheckBox
    Friend WithEvents chkRTUZFiles As System.Windows.Forms.CheckBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblCountdown As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTimerInterval As System.Windows.Forms.TextBox
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents prgTimer As System.Windows.Forms.ProgressBar
    Friend WithEvents time_Progress As System.Windows.Forms.Timer
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents btnProcessSwitch As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblcontact As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents chkTerminations As System.Windows.Forms.CheckBox
    Friend WithEvents chkDataServices As System.Windows.Forms.CheckBox
    Friend WithEvents chkFilesStark As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmailStark As System.Windows.Forms.CheckBox
    Friend WithEvents chkTenantStatments As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents chkUKFixedTerminationRequest As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutomatedReports As System.Windows.Forms.CheckBox
    Friend WithEvents chkSEMOXMLFiles As System.Windows.Forms.CheckBox
    Friend WithEvents chkBudgetRequests_MultiCountry As System.Windows.Forms.CheckBox
    Friend WithEvents chkBudgetRequests_MultiClient As System.Windows.Forms.CheckBox
    Friend WithEvents chkFTPCarillion As System.Windows.Forms.CheckBox
    Friend WithEvents chkFilesCarillionFTP As System.Windows.Forms.CheckBox

End Class
